/*package TD;
import java.awt.Color;
import java.awt.GridLayout;
import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;

import javax.swing.JPanel;

public class PanelEdytor extends JPanel{
	
	int [][] TabMap = new int[10][17];
	
	public PanelEdytor()  throws IOException{
		setLayout(new GridLayout(10,17));
		for(int i = 0; i<10; i++){
			for(int j = 0; j<17; j++){
				TabMap[i][j] = 1;
			}
		}
		setVisible(false);
		
		
	}
	public void ZapiszMape() throws IOException{
		BufferedWriter out = new BufferedWriter(new FileWriter("nowa_mapa"));
		
		for(int i = 0; i<10; i++){
			for(int j = 0; j<17; j++){
				out.write(TabMap[i][j] + " ");
			}
			out.newLine();
		}
		out.flush();
		out.close();
	}
}
*/
package TD;
import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Font;
import java.awt.FontFormatException;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Stroke;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.util.InputMismatchException;
import java.util.NoSuchElementException;
import java.util.Scanner;

import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

import SP.MapFileFilter;

/*
 * 
 * 1 - trawa
 * 2 - sciezka
 * 3 - zajete przez wiezyczke
 * 4 - wejscie
 * 5 - wyjscie 
 * 	ktoryPanel 1 - wyjscie, 2 - wejscie 3 - sciezka 4 - trawa
 */
//setSize(867,540);
public class PanelEdytor extends JPanel implements  MouseMotionListener, MouseListener, ActionListener{
	
	private int [][] TabMap = new int[10][17]; //[y][x]
	
	private int myszx, myszy, dyskretneX, dyskretneY;

	final JFileChooser fc = new JFileChooser();
	private int ktoryPanel = 1;
	Font FontGra;
	
	JButton bZapisz, bOtworz;

	
	public PanelEdytor()  throws IOException{
		for(int i = 0; i<10; i++){
			for(int j = 0; j<17; j++){
				TabMap[i][j] = 1;
			}
		}
		fc.addChoosableFileFilter(new MapFileFilter() );

		try {
			FontGra = Font.createFont(Font.TRUETYPE_FONT, new File("FontGra.ttf")).deriveFont(20f);
			
		} catch (FontFormatException e) {
			JOptionPane.showMessageDialog(null,"Czionka nie zaladowala sie");
			e.printStackTrace();
		}
		setVisible(false);	
		setLayout(null);

		addMouseMotionListener(this);
		addMouseListener(this);
		//
		bZapisz = new JButton("ZAPISZ");
		bZapisz.setBackground(Color.DARK_GRAY);
		bZapisz.setForeground(Color.WHITE);
		bZapisz.setBounds(890, 12, 100, 20);
		bZapisz.setFocusPainted(false);
		bZapisz.setFont(FontGra);
		bZapisz.addActionListener(this);
		this.add(bZapisz);
		//
		//
		bOtworz = new JButton("OTWORZ");
		bOtworz.setBackground(Color.DARK_GRAY);
		bOtworz.setForeground(Color.WHITE);
		bOtworz.setBounds(1000, 12, 100, 20);
		bOtworz.setFocusPainted(false);
		bOtworz.setFont(FontGra);
		bOtworz.addActionListener(this);
		this.add(bOtworz);
		//
		
	}
    @Override
    public void paintComponent(Graphics g) {
        super.paintComponent(g);
        
        
        wyrysujMape(g);
        boczneMenuRysuj(g);
    }

	private void podswietlanieMapy(Graphics2D g2d, Color color, int i,int j){
		g2d.setColor(color);					
		g2d.fillRect(j*50, i *50, 50, 50);
		g2d.setColor(Color.BLACK);
		int thickness = 1;
		Stroke oldStroke = g2d.getStroke();
		g2d.setStroke(new BasicStroke(thickness));
		g2d.drawRect(j*50,i *50, 50, 50);
		g2d.setStroke(oldStroke);
	}
	private void wyrysujMape(Graphics g){	
		
		Graphics2D g2d = (Graphics2D) g;
		for(int i = 0; i<10; i++){
			for(int j = 0; j<17; j++){
				if(TabMap[i][j] == 2){
					podswietlanieMapy(g2d, Color.decode("#A0785A"), i, j);
				}else if(TabMap[i][j] == 4){
					podswietlanieMapy(g2d, Color.decode("#97f4e9"), i, j);
				}else if(TabMap[i][j] == 5){
					podswietlanieMapy(g2d, Color.decode("#98a3f2"), i, j);
				}else{ 
					podswietlanieMapy(g2d, Color.decode("#77DD77"), i, j);
				}
			}	
		}
		int dyskretneX = myszx/50, dyskretneY=myszy/50;
		if(dyskretneX < 17 && dyskretneY < 10 && dyskretneX >= 0 && dyskretneY >= 0){
			if(ktoryPanel == 3){
				podswietlanieMapy(g2d, Color.decode("#A0785A").brighter(), dyskretneY, dyskretneX);
			}else if(ktoryPanel == 2){
				podswietlanieMapy(g2d, Color.decode("#97f4e9").darker(), dyskretneY, dyskretneX);
			}else if(ktoryPanel == 1){
				podswietlanieMapy(g2d, Color.decode("#98a3f2").darker(), dyskretneY, dyskretneX);
			}else if(ktoryPanel == 4){ 
				podswietlanieMapy(g2d, Color.decode("#77DD77").brighter(), dyskretneY, dyskretneX);
			}
		}

	}
	public void ZapiszMape(File file) throws IOException{
		BufferedWriter out = new BufferedWriter(new FileWriter(file));
		
		for(int i = 0; i<10; i++){
			for(int j = 0; j<17; j++){
				out.write(TabMap[i][j] + " ");
			}
			out.newLine();
		}
		out.flush();
		out.close();
	}
	@Override
	public void mouseDragged(MouseEvent e) {
		myszx = e.getX();
		myszy = e.getY();
		dyskretneX = myszx/50;
		dyskretneY = myszy/50;
		ustawPanel(dyskretneX, dyskretneY);
		repaint();
	}
	private void ustawPanel(int dyskretneX, int dyskretneY) {
		if(dyskretneX < 17 && dyskretneY < 10 && dyskretneX >= 0 && dyskretneY >= 0){
			if(ktoryPanel == 1){
				TabMap[dyskretneY][dyskretneX] = 5;
			}else if(ktoryPanel == 2){
				TabMap[dyskretneY][dyskretneX] = 4;
			}else if(ktoryPanel == 3){
				TabMap[dyskretneY][dyskretneX] = 2;
			}else if(ktoryPanel == 4){
				TabMap[dyskretneY][dyskretneX] = 1;
			}
		}
	}
	@Override
	public void mouseMoved(MouseEvent e) {
		myszx = e.getX();
		myszy = e.getY();
		dyskretneX = myszx/50;
		dyskretneY = myszy/50;
		repaint();
		
	}
	@Override
	public void mouseClicked(MouseEvent e) {
		
		dyskretneX = myszx/50;
		dyskretneY = myszy/50;
		zmianaKoloruPisania(dyskretneX,dyskretneY);
		ustawPanel(dyskretneX, dyskretneY);
		repaint();
	}
	private void zmianaKoloruPisania(int dyskretneX2, int dyskretneY2) {
		if(dyskretneX == 18 && dyskretneY == 2){
			ktoryPanel = 1;
		}else if(dyskretneX == 21 && dyskretneY == 2){
			ktoryPanel = 2;
		}else if(dyskretneX == 18 && dyskretneY == 4){
			ktoryPanel = 3;
		}else if(dyskretneX == 21 && dyskretneY == 4){
			ktoryPanel = 4;
		}
	}
	private void boczneMenuRysuj(Graphics g){
		//
		Graphics2D g2d = (Graphics2D) g;
		g2d.setColor(Color.BLACK);
		g2d.fillRect(851, 0, 300, 501);
		
		 
		
		g2d.setFont(FontGra);
		g2d.setColor(Color.white);
		g2d.drawString("meta", 900, 170);
		g2d.drawString("start", 1050, 170);
		g2d.drawString("sciezka", 900, 270);
		g2d.drawString("trawa", 1050, 270);
		
		podswietlanieMapy(g2d, Color.decode("#A0785A"), 4, 18);
		podswietlanieMapy(g2d, Color.decode("#97f4e9"), 2, 21);
		podswietlanieMapy(g2d, Color.decode("#98a3f2"), 2, 18);
		podswietlanieMapy(g2d, Color.decode("#77DD77"), 4, 21);
		g2d.setColor(Color.RED);
		switch (ktoryPanel) {
		case 1:
			g2d.drawRect (900, 100, 50, 50);
			break;
		case 2:
			g2d.drawRect (1050, 100, 50, 50);
			break;
		case 3:
			g2d.drawRect (900, 200, 50, 50);
			break;
		case 4:
			g2d.drawRect (1050, 200, 50, 50);
			break;
		}
	}

 
	@Override
	public void mousePressed(MouseEvent e) {
		
		
	}
	@Override
	public void mouseReleased(MouseEvent e) {
		
	}
	@Override
	public void mouseEntered(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}
	@Override
	public void mouseExited(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}
	public void WczytajMape(File file){
		Scanner scanner = null;
		try {
			scanner = new Scanner(file);
		} catch (FileNotFoundException e) {
			JOptionPane.showMessageDialog(this,"Nieznaleziono pliku");
			System.exit(0);
			e.printStackTrace();
		}
		try{
			for(int i = 0; i<10; i++){
				for(int j = 0; j<17; j++){
					TabMap[i][j] = scanner.nextInt();
				}
			}
		} catch (InputMismatchException e) {
			JOptionPane.showMessageDialog(this,"Niezgodna zawarto�� pliku");
			System.exit(0); 
		} catch (NoSuchElementException e){
			JOptionPane.showMessageDialog(this,"Za malo danych w pliku");
			System.exit(0); 
		}
		scanner.close();
		
	}
	
	
	@Override
	public void actionPerformed(ActionEvent e) {
		Object zrodlo = e.getSource();
		if(zrodlo == bZapisz){

		int wybor = fc.showSaveDialog(this);
			if (wybor == JFileChooser.APPROVE_OPTION) {
	            File file = fc.getSelectedFile();
	            if(!fc.getSelectedFile().getAbsolutePath().endsWith(".map")){
	                file = new File(fc.getSelectedFile() + ".map");
	            }
	            try {
					ZapiszMape(file);
				} catch (IOException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}
			
		}
		else if(zrodlo == bOtworz){
			int wybor = fc.showOpenDialog(this);
			if (wybor == JFileChooser.APPROVE_OPTION) {
	            File file = fc.getSelectedFile();
	            WczytajMape(file);
	        }
		}
		
		repaint();
	}
}
