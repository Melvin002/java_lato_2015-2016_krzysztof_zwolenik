package TD;
import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.Stroke;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.Serializable;

import javax.swing.ImageIcon;
import javax.swing.JPanel;

public class Potworek implements Serializable{


	public int id;
    private int x;
    private int y;
    private transient Image image;
    int zycie, zycieMax, predkosc, skadidzie;
    // kierunki 1 - lewo 2 - gora  3 - prawo 4 - dol
    public enum TypPotworka{
    	BOSS, LATAJACY, ZIEMNY
    }
    private TypPotworka typPotworka;
    public Potworek(TypPotworka typPotworka) {
        this.typPotworka = typPotworka;
        x = 0;
        y = 2 * 50; 
        initP();
    }
    public Potworek(TypPotworka typPotworka, int id) {
        this.typPotworka = typPotworka;
        x = 0;
        y = 2 * 50; 
        initP();
        this.id = id;
    }
    
    public void initP() {
        ImageIcon ii = null;
        switch (typPotworka){
    	case BOSS:
    		ii = new ImageIcon("ludzik1.png");
    		image = ii.getImage();
    		predkosc = 1;
    		zycieMax = zycie = 5000;
    		break;
    	case LATAJACY:
    		ii = new ImageIcon("ludzik2.png");
    		image = ii.getImage();
    		predkosc = 4;
    		zycieMax = zycie = 100;
    		break;

    	case ZIEMNY:
    		ii = new ImageIcon("ludzik3.png");
    		image = ii.getImage();
    		predkosc = 3;
    		zycieMax = zycie = 250;
    		break;
    	}
    }
    public boolean move(int TabMap[][]){
    	int ix = x, iy = y, przesuniecie_x = 0, przesuniecie_y = 0;
    	for(int i = 0; i < predkosc; i++){
    		if(TabMap[iy/50][ix/50] == 5 && TabMap[(iy + 49)/50][(ix + 49)/50] == 5){
				 zycie = -1;
				 return true;
			 }
    		if(skadidzie != 4 && skadidzie != 2 && iy/50 < 9 && ((TabMap[iy/50 + 1][ix/50] == 2 && TabMap[(iy + 49)/50 + 1][(ix + 49)/50] == 2) || (TabMap[iy/50 + 1][ix/50] == 5 && TabMap[(iy + 49)/50 + 1][(ix + 49)/50] == 5))){
				 setZkadidzie(2);
			 }
			 else if(skadidzie != 3 && skadidzie != 1 && ix/50 <= 15 && ((TabMap[iy/50][ix/50 + 1] == 2 && TabMap[(iy + 49)/50][(ix + 49)/50 + 1] == 2) || (TabMap[iy/50][ix/50 + 1] == 5 && TabMap[(iy + 49)/50][(ix + 49)/50 + 1] == 5))){
				 setZkadidzie(1);
			 }
		 	else if(skadidzie != 2 && skadidzie != 4 && iy/50 > 0 && ((TabMap[iy/50 - 1][ix/50] == 2 && TabMap[(iy + 49)/50 - 1][(ix + 49)/50] == 2) || (TabMap[iy/50 - 1][ix/50] == 5 && TabMap[(iy + 49)/50 - 1][(ix + 49)/50] == 5))){
			 	setZkadidzie(4);
		 	}
		 	else if(skadidzie != 1 && skadidzie != 3 && ix/50 > 0 && ((TabMap[iy/50][ix/50 - 1] == 2 && TabMap[(iy + 49)/50][(ix + 49)/50 - 1] == 2) || (TabMap[iy/50][ix/50 - 1] == 5 && TabMap[(iy + 49)/50][(ix + 49)/50 - 1] == 5))){
			 	setZkadidzie(3);
		 	}
    		if(skadidzie == 1){ // z lewej
    			ix++;
    			przesuniecie_x++;
    		}
    		else if(skadidzie == 2){ // z gory 
    			iy++;
    			przesuniecie_y++;
    		}
    		else if(skadidzie == 3){ // z prawej
    			ix--;
    			przesuniecie_x--;
    		}
    		else if(skadidzie == 4){ // z dolu
    			iy--;
    			przesuniecie_y--;
    		}
    	}
        x += przesuniecie_x;
        y += przesuniecie_y;
        return false;
    }
    public int getX() {
        return x;
    }
    public int getSkadidzie(){
    	return skadidzie;
    }
    public int getY() {
        return y;
    }

    public Image getImage() {
        return image;
    }
    public int getKierunek() {
        return skadidzie;
    }
    public void setZkadidzie(int wart) {
        skadidzie = wart;
    }
    public void obrazenia(int wartoscAtaku){
    	zycie -= wartoscAtaku;
    }
    public TypPotworka getTyp(){
    	return this.typPotworka;
    }
    public int getZycie() {
        return zycie;
    }
    public void setZycie(int zycie) {
        this.zycie = zycie;
    }
    public int getPredkosc(){
    	return predkosc;
    }
    private double procentZycia(){
    	double procent = (double)zycie/zycieMax;
    	return procent;
    }
    public void rysujPotworka(Graphics g, JPanel panel){
    	Graphics2D g2d = (Graphics2D) g;
    	
    	g2d.drawImage(getImage(), getX() ,getY() , panel);
    	
		g2d.setColor(Color.decode("#ff0000"));
		g2d.fillRect(getX() + 5, getY() - 10, (int)(40 * procentZycia()), 10);
		g2d.setColor(Color.BLACK);
		int thickness = 1;
		Stroke oldStroke = g2d.getStroke();
		g2d.setStroke(new BasicStroke(thickness));
		g2d.drawRect(getX() + 5,getY() - 10, (int)(40 * procentZycia()), 10);
		g2d.setStroke(oldStroke);
    }
	private void readObject(ObjectInputStream in){
		try {
			in.defaultReadObject();
		} catch (ClassNotFoundException | IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		initP();
	}
	@Override
    public boolean equals(Object obj) {
        if (obj == null)
            return false;
        if (obj == this)
        	return true;
        if (obj instanceof Potworek){
            Potworek w = (Potworek)obj;
            if(w.id == this.id){
            	return true;
            }
        }
        return false;
    }
}