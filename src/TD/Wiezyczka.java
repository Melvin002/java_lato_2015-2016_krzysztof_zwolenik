package TD;
import java.awt.Image;
import java.awt.Point;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Iterator;

import javax.swing.ImageIcon;

import MP.PakietObrazenia;
import MP.PakietZegar;
import MP.ServerGry;
import MP.WatekUDP;

public class Wiezyczka implements Serializable{

	public int id;
    private int x;
    private int y;
    private transient Image image;
    private int zasieg, poziom, pktpoziomu, obrazenia, atakCoIleIteracji, licznikataku;
    private TrybStrzelania trybStrzelania;

	private TypWierzyczki typWierzyczki;
    public enum TypWierzyczki{
    	DZIALO, KARABIN, SPOWALNIACZ
    }
    public enum TrybStrzelania {
        NAJBLIZSZY, NAJWIECEJZYCIA, NAJMNIEJZYCIA, NAJSZYBSZY, NAJWOLNIEJSZY 
    }

    public Wiezyczka(int i, int j, TypWierzyczki jakityp) {
    	trybStrzelania = TrybStrzelania.NAJBLIZSZY;
    	poziom = 1;
        x = i * 50 + 8;
        y = j * 50 + 8;    
        init(jakityp);
    }
    public Wiezyczka(int i, int j, TypWierzyczki jakityp, int id) {
    	trybStrzelania = TrybStrzelania.NAJBLIZSZY;
    	poziom = 1;
        x = i * 50 + 8;
        y = j * 50 + 8;    
        init(jakityp);
        this.id = id;
    }
    
    private void init(TypWierzyczki jakityp) {
        
    	setTypWierzyczki(jakityp);
    	ImageIcon ii = null;
    	switch (getTypWierzyczki()){
    	case DZIALO:
    		ii = new ImageIcon("wieza1.jpg");
    		image = ii.getImage();
    		zasieg = 100;
    		obrazenia = 50;
    		licznikataku = atakCoIleIteracji = 40;
    		break;
    	case KARABIN:
    		ii = new ImageIcon("wieza2.jpg");
    		image = ii.getImage();
    		zasieg = 200;
    		obrazenia = 30;
    		licznikataku = atakCoIleIteracji = 40;
    		break;

    	case SPOWALNIACZ:
    		ii = new ImageIcon("wieza3.jpg");
    		image = ii.getImage();
    		zasieg = 300;
    		obrazenia = 10;
    		licznikataku = atakCoIleIteracji = 40;
    		break;
    	}
        
    
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    public Image getImage() {
        return image;
    }
    public int getObrazenia() {
    	return obrazenia;
    }
    public int getPrdkatak() {
    	return atakCoIleIteracji;
    }
    public int getZasieg() {
    	return zasieg;
    }
    public void setTrybStrzelania(TrybStrzelania trybStrzelania) {
		this.trybStrzelania = trybStrzelania;
	}
	public TrybStrzelania getTrybStrzelania() {
		return trybStrzelania;
	}
    public void WarunekZniszczenia(ArrayList <Potworek> plist, ArrayList <Point> punktyLiniAtaku){
		double wyliczonaOdleglosc, najkrotszaOdleglosc = 99999999;
		int zycieNAJMNIEJ = 9999999;
		int zycieNAJWIECEJ = 0;
		int najszybszy = 0,najwolniejszy = 99999;
		Potworek potworekKtoryOtrzymaObrazenia = null;
		Iterator <Potworek> i = plist.iterator();
		while(i.hasNext()){
			Potworek p = (Potworek) i.next();
			wyliczonaOdleglosc = (Math.sqrt((Math.pow((double)(getX()-(p.getX()+25)),2.0) + Math.pow((double)(getY() - (p.getY()+25)),2))));
			if(wyliczonaOdleglosc < getZasieg()){
				switch(trybStrzelania){
				case NAJBLIZSZY:
					if(najkrotszaOdleglosc > wyliczonaOdleglosc){
						najkrotszaOdleglosc = wyliczonaOdleglosc;
						potworekKtoryOtrzymaObrazenia = p;
					}
					break;
				case NAJMNIEJZYCIA:
					if(zycieNAJMNIEJ > p.getZycie()){
						zycieNAJMNIEJ = p.getZycie();
						potworekKtoryOtrzymaObrazenia = p;
					}
					break;
				case NAJWIECEJZYCIA:
					if(zycieNAJWIECEJ <= p.getZycie()){
						zycieNAJWIECEJ = p.getZycie();
						potworekKtoryOtrzymaObrazenia = p;
					}
					break;
				case NAJSZYBSZY:
					if(najszybszy < p.getPredkosc()){
						najszybszy = p.getPredkosc();
						potworekKtoryOtrzymaObrazenia = p;
					}
					break;
				case NAJWOLNIEJSZY:
					if(najwolniejszy > p.getPredkosc()){
						najwolniejszy = p.getPredkosc();
						potworekKtoryOtrzymaObrazenia = p;
					}
					break;
				}
			}
		}
		if(potworekKtoryOtrzymaObrazenia != null && licznikataku == atakCoIleIteracji){
			potworekKtoryOtrzymaObrazenia.obrazenia(getObrazenia());
			punktyLiniAtaku.add(new Point(getX(),getY()));
			punktyLiniAtaku.add(new Point(potworekKtoryOtrzymaObrazenia.getX(),potworekKtoryOtrzymaObrazenia.getY()));
			licznikataku = 0;
		}
		
		if(licznikataku != atakCoIleIteracji){
			licznikataku++;
		}
		
    }
    //wersja mp
    public void WarunekZniszczenia(ArrayList <Potworek> plist, ArrayList <Point> punktyLiniAtaku, WatekUDP server){
		double wyliczonaOdleglosc, najkrotszaOdleglosc = 99999999;
		int zycieNAJMNIEJ = 9999999;
		int zycieNAJWIECEJ = 0;
		int najszybszy = 0,najwolniejszy = 99999;
		Potworek potworekKtoryOtrzymaObrazenia = null;
		Iterator <Potworek> i = plist.iterator();
		while(i.hasNext()){
			Potworek p = (Potworek) i.next();
			wyliczonaOdleglosc = (Math.sqrt((Math.pow((double)(getX()-(p.getX()+25)),2.0) + Math.pow((double)(getY() - (p.getY()+25)),2))));
			if(wyliczonaOdleglosc < getZasieg()){
				switch(trybStrzelania){
				case NAJBLIZSZY:
					if(najkrotszaOdleglosc > wyliczonaOdleglosc){
						najkrotszaOdleglosc = wyliczonaOdleglosc;
						potworekKtoryOtrzymaObrazenia = p;
					}
					break;
				case NAJMNIEJZYCIA:
					if(zycieNAJMNIEJ > p.getZycie()){
						zycieNAJMNIEJ = p.getZycie();
						potworekKtoryOtrzymaObrazenia = p;
					}
					break;
				case NAJWIECEJZYCIA:
					if(zycieNAJWIECEJ <= p.getZycie()){
						zycieNAJWIECEJ = p.getZycie();
						potworekKtoryOtrzymaObrazenia = p;
					}
					break;
				case NAJSZYBSZY:
					if(najszybszy < p.getPredkosc()){
						najszybszy = p.getPredkosc();
						potworekKtoryOtrzymaObrazenia = p;
					}
					break;
				case NAJWOLNIEJSZY:
					if(najwolniejszy > p.getPredkosc()){
						najwolniejszy = p.getPredkosc();
						potworekKtoryOtrzymaObrazenia = p;
					}
					break;
				}
			}
		}
		if(potworekKtoryOtrzymaObrazenia != null && licznikataku == atakCoIleIteracji){
			potworekKtoryOtrzymaObrazenia.obrazenia(getObrazenia());
			PakietObrazenia pakiet = new PakietObrazenia(potworekKtoryOtrzymaObrazenia.id, potworekKtoryOtrzymaObrazenia.getZycie());
			pakiet.wyslijPakiet(ServerGry.server);
			punktyLiniAtaku.add(new Point(getX(),getY()));
			punktyLiniAtaku.add(new Point(potworekKtoryOtrzymaObrazenia.getX(),potworekKtoryOtrzymaObrazenia.getY()));
			licznikataku = 0;
		}
		
		if(licznikataku != atakCoIleIteracji){
			licznikataku++;
		}
		
    }

	public TypWierzyczki getTypWierzyczki() {
		return typWierzyczki;
	}

	public void setTypWierzyczki(TypWierzyczki typWierzyczki) {
		this.typWierzyczki = typWierzyczki;
	}
	private void readObject(ObjectInputStream in){
		try {
			in.defaultReadObject();
		} catch (ClassNotFoundException | IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		init(this.typWierzyczki);
	}
	@Override
    public boolean equals(Object obj) {
        if (obj == null)
            return false;
        if (obj == this)
        	return true;
        if (obj instanceof Wiezyczka){
            Wiezyczka w = (Wiezyczka)obj;
            if(w.getX() == this.getX() && w.getY() == this.getY() && w.getTypWierzyczki() == this.getTypWierzyczki()){
            	return true;
            }
        }
        return false;
    }
	
}