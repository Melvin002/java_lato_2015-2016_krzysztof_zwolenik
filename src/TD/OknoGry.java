package TD;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Font;
import java.awt.HeadlessException;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

import MP.KlientGry;
import MP.ServerGry;
import MP.StronaClienta;
import MP.StronaSerwera;
import SP.PanelGra;
import SP.PanelGraJednoosobowa;

public class OknoGry extends JFrame implements ActionListener, KeyListener{
	
	public PanelGra panel_gra;
	StronaClienta gracz;
	PanelEdytor panel_edytor;
	JPanel panel_menu, panel_ranking, panel_opcje, panel_graJ;
	JButton bGraJednoosobowa, bRanking, bWyjscie, bGraWieloosobowa, bOpcje, bEdytorMap;
	public StronaSerwera stronaSerwera;

	private KlientGry client;
	private ServerGry server;
	
	public OknoGry() throws IOException{
		//
		gracz = null;
		//
		setResizable(false);
		setLayout(null);
		setFocusable(true);
		setSize(853,480);
		setTitle("Krzysztof Zwolenik - Tower Defense");
		//
		addKeyListener(this);
		//


		//

		//panel_menu
		panel_menu = new JPanel();
		panel_menu.setLayout(null);
		JLabel background = new JLabel(new ImageIcon("T�oTD.png"));
		background.setBounds(0, 0, 853, 480);
		
		panel_menu.setVisible(true);
		panel_menu.setBounds(0, 0, 853, 480);
		add(panel_menu);
		
		//panel gry jednosobowej logowanie rejestracja
		panel_graJ = new PanelGraJednoosobowa(this);
		add(panel_graJ);
		
		
		bGraJednoosobowa = dodajPrzycisk("GRA JEDNOOSOBOWA", 0);
		bGraWieloosobowa = dodajPrzycisk("GRA WIELOOSOBOWA", 1);
		bRanking = dodajPrzycisk("RANKING", 2);
		bOpcje = dodajPrzycisk("OPCJE", 3);
		bWyjscie = dodajPrzycisk("WYJSCIE", 5);
		
		
		//panel_ranking
		panel_ranking = new JPanel();
		panel_ranking.setBounds(0, 0, 853, 480);
		panel_ranking.setLayout(null);

		
		JLabel background_ranking = new JLabel(new ImageIcon("ranking.jpg"));
		background_ranking.setBounds(0, 0, 853, 480);
		dodajNapis("Ranking Graczy", 276, 0, 35);

		ArrayList<String> rank = (ArrayList<String>) Files.readAllLines(Paths.get("ranking.txt"));
		
		int i = 1;
		for(String x:rank){ 
			dodajNapis(i + ". " + rank.get(i - 1), 276, (i * 25), 25);
			i++;
		}

		
		panel_ranking.add(background_ranking);
		add(panel_ranking);
		panel_ranking.setVisible(false);
		//panel_opcje
		panel_opcje = new JPanel();
		panel_opcje.setBounds(0, 0, 853, 480);
		panel_opcje.setLayout(new BorderLayout());
		bEdytorMap = new JButton("EDYTOR MAP");
		bEdytorMap.setBackground(Color.DARK_GRAY);
		bEdytorMap.setForeground(Color.WHITE);
		bEdytorMap.setFocusPainted(false);
		bEdytorMap.setFont(new Font("Arial", Font.BOLD, 20));
		bEdytorMap.addActionListener(this);
		panel_opcje.add(bEdytorMap, BorderLayout.CENTER);
		add(panel_opcje);
		panel_opcje.setVisible(false);
		
		panel_menu.add(background);
	}
	private void dodajNapis(String nazwa, int x, int y, int rozm){
		JLabel napis = new JLabel(nazwa);
		napis.setBounds(x, y, 300, 50); //(276, 0, 300, 50
		napis.setFont(new Font("Courier New",Font.BOLD,rozm));
		napis.setForeground(Color.WHITE);
		panel_ranking.add(napis);
	}
	private JButton dodajPrzycisk(String nazwa, int pozycja){
		JButton button;
		button = new JButton(nazwa);
		button.setBackground(Color.DARK_GRAY);
		button.setForeground(Color.WHITE);
		button.setBounds(291, 200 + pozycja * 40, 270, 30);
		button.setFocusPainted(false);
		button.setFont(new Font("Arial", Font.BOLD, 20));
		button.addActionListener(this);
		panel_menu.add(button);
		return button;
	}

	@Override
	public void keyTyped(KeyEvent e) {
		// TODO Auto-generated method stub
		
	}


	@Override
	public void keyPressed(KeyEvent e) {
		int c = e.getKeyCode();
		if(c == KeyEvent.VK_ESCAPE){
			setSize(853,480);
			panel_menu.setVisible(true);
			if(panel_gra != null)
				panel_gra.setVisible(false);
			panel_opcje.setVisible(false);
			panel_ranking.setVisible(false);
			if(panel_edytor != null)
				panel_edytor.setVisible(false);
		}
		
	}
	public void sp(int id, String fileS){
		try {
			panel_gra = new PanelGra(id, fileS);
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		panel_gra.setBounds(0, 0, 1151, 501);
		add(panel_gra);
		setSize(1157,530);
		panel_menu.setVisible(false);
		panel_gra.setVisible(true);
	}

	@Override
	public void keyReleased(KeyEvent e) {
		// TODO Auto-generated method stub
		
	}
	
	@Override
	public void actionPerformed(ActionEvent e) {
		Object zrodlo = e.getSource();
		if (zrodlo == bGraJednoosobowa){
			panel_menu.setVisible(false);
			panel_graJ.setVisible(true);
		}
		else if(zrodlo == bGraWieloosobowa){
			int wybor =JOptionPane.showConfirmDialog(this, "Uruchomic serwer?");
			if(wybor == 0){
				try {
					stronaSerwera = new StronaSerwera();
				} catch (IOException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				server = new ServerGry(stronaSerwera);
			}
			if(wybor == 2)
				return;
			
				client = new KlientGry();
				gracz = client.stronaClienta;
				gracz.setBounds(0, 0, 1151, 501);
				add(gracz);
				setSize(1157,530);
				panel_menu.setVisible(false);
				gracz.setVisible(true);
				
		}
		else if(zrodlo == bRanking){
			panel_menu.setVisible(false);
			panel_ranking.setVisible(true);
		}
		else if(zrodlo == bWyjscie)
			System.exit(0);
		else if(zrodlo == bOpcje){
			panel_menu.setVisible(false);
			panel_opcje.setVisible(true);
		}
		else if(zrodlo == bEdytorMap){
			try {
				panel_edytor = new PanelEdytor();
			} catch (IOException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			panel_edytor.setBounds(0, 0, 1151, 501);
			add(panel_edytor);
			setSize(1157,530);
			panel_opcje.setVisible(false);
			panel_edytor.setVisible(true);
			panel_menu.setVisible(false);

		}
		
	}
}
