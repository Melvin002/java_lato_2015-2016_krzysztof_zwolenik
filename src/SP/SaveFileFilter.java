package SP;

import java.io.File;

import javax.swing.filechooser.FileFilter;

public class SaveFileFilter extends FileFilter {

	@Override
	public boolean accept(File f) {
		if (f.isDirectory()) {
            return true;
        }
        if (f.getAbsolutePath().endsWith(".save")){
                return true;
        } else {
            return false;
        }
	}

	@Override
	public String getDescription() {
		return ".save, zapisy gry TD";
	}

}
