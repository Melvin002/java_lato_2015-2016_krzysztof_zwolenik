package SP;
import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Font;
import java.awt.FontFormatException;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.Point;
import java.awt.Stroke;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.io.Serializable;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.InputMismatchException;
import java.util.Iterator;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Scanner;
import java.util.Stack;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;
import javax.swing.Timer;

import TD.Potworek;
import TD.Wiezyczka;

//setSize(867,540);
public class PanelGra extends JPanel implements Serializable, MouseMotionListener, MouseListener, ActionListener{
	
	private int [][] TabMap = new int[10][17]; //[y][x]
	private transient boolean stanDAD = false;
	private transient int myszx, myszy, dyskretneX, dyskretneY;
	ArrayList<Wiezyczka> wiezyczki;
	ArrayList<Potworek> potworki;
	ArrayList<Point> punktyLiniAtaku;
	transient final JFileChooser fc = new JFileChooser();
	transient Wiezyczka.TypWierzyczki jakityp;
	transient Wiezyczka.TrybStrzelania trybStrzelania;
	transient Wiezyczka WiezyczkaWmenu = null;
	transient Font FontGra;
	Stack<String> stosikPrzeciwnikow;
	transient int licznikiteracji = 1; //wydluzenie czasu wyswietlania sie promieni laserow
	int zegar = 1200; // czas w sekundach= wart/40
	transient private Timer timer;
	int pieniadze, serduszka, punkty;
	transient Image image1, image2, image3, serce, clock, dollar, score;
	transient JButton bStart,bZapisz, bOtworz;
	ArrayList<String> przeciwnicy;
	transient int id;
	transient boolean nowa = true; // cos zwiazne z zapisem gry
	
	public PanelGra(int id, String file)  throws IOException{
		fc.addChoosableFileFilter(new SaveFileFilter() );
		this.id = id;
		try {
//			InputStream in = getClass().getResourceAsStream("res/FontGra.ttf");
//			FontGra = Font.createFont(Font.TRUETYPE_FONT, in);
			FontGra = Font.createFont(Font.TRUETYPE_FONT, new File("FontGra.ttf")).deriveFont(20f);
			
		} catch (FontFormatException e) {
			JOptionPane.showMessageDialog(null,"Czionka nie zaladowala sie");
			e.printStackTrace();
		}
		jakityp = Wiezyczka.TypWierzyczki.DZIALO;
		setVisible(false);	
		setLayout(null);
		if(file != null)
			WczytajMape(file);
		addMouseMotionListener(this);
		addMouseListener(this);
		wiezyczki = new ArrayList<Wiezyczka>();
		potworki = new ArrayList<Potworek>();
		punktyLiniAtaku = new ArrayList<Point>();
		stosikPrzeciwnikow = new Stack<String>();
		if(file != null)
			wczytajPrzeciwnikow(1);
		pieniadze = 1000;
		serduszka = 10;
		//
		bStart = new JButton("START");
		bStart.setBackground(Color.DARK_GRAY);
		bStart.setForeground(Color.WHITE);
		bStart.setBounds(1000, 12, 80, 20);
		bStart.setFocusPainted(false);
		bStart.setFont(FontGra);
		bStart.addActionListener(this);
		this.add(bStart);
		//
		bZapisz = new JButton("ZAPISZ");
		bZapisz.setBackground(Color.DARK_GRAY);
		bZapisz.setForeground(Color.WHITE);
		bZapisz.setBounds(1000, 32, 80, 20);
		bZapisz.setFocusPainted(false);
		bZapisz.setFont(FontGra);
		bZapisz.addActionListener(this);
		this.add(bZapisz);
		//
		//
		bOtworz = new JButton("OTWORZ");
		bOtworz.setBackground(Color.DARK_GRAY);
		bOtworz.setForeground(Color.WHITE);
		bOtworz.setBounds(1080, 12, 80, 20);
		bOtworz.setFocusPainted(false);
		bOtworz.setFont(FontGra);
		bOtworz.addActionListener(this);
		this.add(bOtworz);
		//
        timer = new Timer(25, this);
        timer.start();
		ImageIcon ii1 = new ImageIcon("clock.png");
		clock = ii1.getImage();
		ImageIcon ii2 = new ImageIcon("heart.png");
		serce = ii2.getImage();
		ImageIcon ii3 = new ImageIcon("dollar.png");
		dollar = ii3.getImage();
		ImageIcon ii4 = new ImageIcon("score.png");
		score = ii4.getImage();
		
	}
	private void wczytajPrzeciwnikow(int i) throws IOException{
		String line = przeciwnicy.get(i - 1);
		Scanner scanner = new Scanner(line);
		while(scanner.hasNext()){
				stosikPrzeciwnikow.add(scanner.next());
		}
		scanner.close();
	}
    @Override
    public void paintComponent(Graphics g) {
        super.paintComponent(g);
        
        
        wyrysujMape(g);
        boczneMenuRysuj(g);
        rysujWiezyczkiPotworki(g);
        zasiegWiez(g);
        if(stanDAD == true){
        	rysujWidmoWiezyczki(g, jakityp, myszx, myszy);
        	repaint();
        }
        atak(g);
    }
    private void atak(Graphics g){
		Iterator <Point> punkt = punktyLiniAtaku.iterator();
		while(punkt.hasNext()){
			licznikiteracji++;
			Point p = (Point) punkt.next();
			int tmpx = p.x + 17;
			int tmpy = p.y + 17;
			if(licznikiteracji % 20 == 0)
				punkt.remove();
			p = (Point) punkt.next();
			liniaAtaku(g, tmpx, tmpy, p.x + 25, p.y + 25);
			if(licznikiteracji % 20 == 0){
				punkt.remove();
				licznikiteracji=0;
			}
		}
		
    }
    public void liniaAtaku(Graphics g, int x1, int y1, int x2, int y2){
    	Graphics2D g2d = (Graphics2D) g;
    	
    	g2d.setStroke(new BasicStroke(10));
    	g2d.setColor(new Color(254, 73, 2, 180));
    	g2d.drawLine(x1, y1, x2, y2);
    }
    private void rysujWidmoWiezyczki(Graphics g, Wiezyczka.TypWierzyczki jakityp, int x, int y){
    	Image image = null;
    	switch (jakityp){
    	case DZIALO:
    		image = new ImageIcon("wieza1.jpg").getImage();
    		break;
    	case KARABIN:
    		image = new ImageIcon("wieza2.jpg").getImage();
    		break;

    	case SPOWALNIACZ:
    		image = new ImageIcon("wieza3.jpg").getImage();
    		break;
    	}
    	Graphics2D g2d = (Graphics2D) g;
    	g2d.drawImage(image, x - 20,y - 20, this);
    }
    private void rysujWiezyczkiPotworki(Graphics g) {
		Graphics2D g2d = (Graphics2D) g;
		for(Wiezyczka m:wiezyczki){
			g2d.drawImage(m.getImage(), m.getX() ,m.getY() , this);
		}
		for(Potworek m:potworki){
			m.rysujPotworka(g2d, this);
			repaint();
		}
		
	}
	private void zasiegWiez(Graphics g){
		
		int tempx = myszx/50;
		int tempy = myszy/50;
		Graphics2D g2d = (Graphics2D) g;
		Iterator <Wiezyczka> i = wiezyczki.iterator();
		while(i.hasNext()){
			Wiezyczka m = (Wiezyczka) i.next();
			if(tempx == m.getX()/50 && tempy == m.getY()/50){
				g2d.setColor(new Color(255, 153, 0, 120));	
				g2d.fillOval(tempx * 50 - (m.getZasieg() * 2-50)/2, tempy * 50 - (m.getZasieg() * 2-50)/2, m.getZasieg()*2, m.getZasieg()*2);					
			}
		}		
	}
	private void podswietlanieMapy(Graphics2D g2d, Color color, int i,int j){
		g2d.setColor(color);					
		g2d.fillRect(j*50, i *50, 50, 50);
		g2d.setColor(Color.BLACK);
		int thickness = 1;
		Stroke oldStroke = g2d.getStroke();
		g2d.setStroke(new BasicStroke(thickness));
		g2d.drawRect(j*50,i *50, 50, 50);
		g2d.setStroke(oldStroke);
	}
	private void wyrysujMape(Graphics g){	
		
		Graphics2D g2d = (Graphics2D) g;
		for(int i = 0; i<10; i++){
			for(int j = 0; j<17; j++){
				if(TabMap[i][j] == 2 || TabMap[i][j] == 4 || TabMap[i][j] == 5){
					podswietlanieMapy(g2d, Color.decode("#A0785A"), i, j);

				}else{ 
					podswietlanieMapy(g2d, Color.decode("#77DD77"), i, j);
				}
			}	
		}
		int dyskretneX = myszx/50, dyskretneY=myszy/50;
		if(dyskretneX < 17 && dyskretneY < 10 && dyskretneX >= 0 && dyskretneY >= 0){
			if(TabMap[dyskretneY][dyskretneX] == 2){
				podswietlanieMapy(g2d, Color.decode("#ff4444"), dyskretneY, dyskretneX);

			}else{ 
				podswietlanieMapy(g2d, Color.decode("#81d8d0"), dyskretneY, dyskretneX);
			}
			
		}

	}
	public void WczytajMape(String file){
		try {
			przeciwnicy = (ArrayList<String>) Files.readAllLines(Paths.get("Poziomy"));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		Scanner scanner = null;
		try {
			scanner = new Scanner(new File(file));
		} catch (FileNotFoundException e) {
			JOptionPane.showMessageDialog(this,"Nieznaleziono pliku");
			System.exit(0);
			e.printStackTrace();
		}
		try{
			for(int i = 0; i<10; i++){
				for(int j = 0; j<17; j++){
					TabMap[i][j] = scanner.nextInt();
				}
			}
		} catch (InputMismatchException e) {
			JOptionPane.showMessageDialog(this,"Niezgodna zawarto�� pliku");
			System.exit(0); 
		} catch (NoSuchElementException e){
			JOptionPane.showMessageDialog(this,"Za malo danych w pliku");
			System.exit(0); 
		}
		scanner.close();
		
	}
	@Override
	public void mouseDragged(MouseEvent e) {
		myszx = e.getX();
		myszy = e.getY();
		dyskretneX = myszx/50;
		dyskretneY = myszy/50;
		if(stanDAD == false && myszx > 884 && myszx < 919 && myszy > 50 && myszy < 100){
			stanDAD = true;
			jakityp = Wiezyczka.TypWierzyczki.DZIALO;
		}
		else if(stanDAD == false && myszx > 983 && myszx < 1019 && myszy > 50 && myszy < 100){
			stanDAD = true;
			jakityp = Wiezyczka.TypWierzyczki.KARABIN;
		}
		else if(stanDAD == false && myszx > 1084 && myszx < 1119 && myszy > 50 && myszy < 100){
			stanDAD = true;
			jakityp = Wiezyczka.TypWierzyczki.SPOWALNIACZ;
		}
		
	}
	@Override
	public void mouseMoved(MouseEvent e) {
		myszx = e.getX();
		myszy = e.getY();
		
	}
	@Override
	public void mouseClicked(MouseEvent e) {
		dyskretneX = myszx/50;
		dyskretneY = myszy/50;
		boczneMenuObsluga(dyskretneX,dyskretneY);
		
        {
    		Iterator <Wiezyczka> i = wiezyczki.iterator();
    		while(i.hasNext()){
    			Wiezyczka m = (Wiezyczka) i.next();
    			if(SwingUtilities.isRightMouseButton(e) && dyskretneX == m.getX()/50 && dyskretneY == m.getY()/50){
    				TabMap[dyskretneY][dyskretneX] = 1;
    				i.remove();
    				pieniadze += 100;
    			}
    			else if(SwingUtilities.isLeftMouseButton(e) && dyskretneX == m.getX()/50 && dyskretneY == m.getY()/50){
    				WiezyczkaWmenu = m;
    			}
    		}
        }
        
       int x1 = 884, y1 = 100;
       if(myszx > x1 && myszx < x1 + 200 && myszy > y1 && myszy < y1 + 20){
        	WiezyczkaWmenu.setTrybStrzelania(Wiezyczka.TrybStrzelania.NAJBLIZSZY);
       }
       else if(myszx > x1 && myszx < x1 + 200 && myszy > (y1 + 90) && myszy < (y1 + 90) + 20){
          	WiezyczkaWmenu.setTrybStrzelania(Wiezyczka.TrybStrzelania.NAJWIECEJZYCIA);
       }
       else if(myszx > x1 && myszx < x1 + 200 && myszy > (y1 + 120) && myszy < (y1 + 120) + 20){
          	WiezyczkaWmenu.setTrybStrzelania(Wiezyczka.TrybStrzelania.NAJMNIEJZYCIA);
       }
       else if(myszx > x1 && myszx < x1 + 200 && myszy > (y1 + 150) && myszy < (y1 + 150) + 20){
          	WiezyczkaWmenu.setTrybStrzelania(Wiezyczka.TrybStrzelania.NAJSZYBSZY);
       }
       else if(myszx > x1 && myszx < x1 + 200 && myszy > (y1 + 180) && myszy < (y1 + 180) + 20){
          	WiezyczkaWmenu.setTrybStrzelania(Wiezyczka.TrybStrzelania.NAJWOLNIEJSZY);
       }
        

	}
	private void boczneMenuObsluga(int dyskretneX,int dyskretneY){

	}
	private void boczneMenuRysuj(Graphics g){
		//
		Graphics2D g2d = (Graphics2D) g;
		g2d.setColor(Color.BLACK);
		g2d.fillRect(851, 0, 300, 501);
		//czarny prostok�t t�o
		//
		ImageIcon ii1 = new ImageIcon("wieza1.jpg");
		image1 = ii1.getImage();
		ImageIcon ii2 = new ImageIcon("wieza2.jpg");
		image2 = ii2.getImage();
		ImageIcon ii3 = new ImageIcon("wieza3.jpg");
		image3 = ii3.getImage();
		g2d.drawImage(image1, 884 ,50 , this);
		g2d.drawImage(image2, 983 ,50 , this);
		g2d.drawImage(image3, 1084 ,50 , this);
		//wierzyczki
		//butonki
		if(WiezyczkaWmenu != null){
			int wys = 100;
			if(WiezyczkaWmenu.getTrybStrzelania() == Wiezyczka.TrybStrzelania.NAJBLIZSZY){
				butonek(g, 884, wys, "NAJBLIZSZY", Color.lightGray);
			}
			else
			butonek(g, 884, wys, "NAJBLIZSZY", Color.white);
			butonek(g, 884, wys + 30, "NAJTRUNIEJSZY",Color.white);
			butonek(g, 884, wys + 60, "NAJLATWIEJSZY",Color.white);
			butonek(g, 884, wys + 90, "NAJWIECEJZYCIA",Color.white);
			butonek(g, 884, wys + 120, "NAJMNIEJZYCIA",Color.white);
			butonek(g, 884, wys + 150, "NAJSZYBSZY",Color.white);
			butonek(g, 884, wys + 180, "NAJWOLNIEJSZY",Color.white);
		}
		g2d.setColor(Color.white);
		g2d.setFont(FontGra);
		g2d.drawImage(clock, 850 ,0 , this);
		g2d.drawString(String.valueOf(zegar/40), 880, 20);	
		g2d.drawImage(serce, 850 ,25 , this);
		g2d.drawString(String.valueOf(serduszka), 880, 45);
		g2d.drawImage(dollar, 910 ,0 , this);
		g2d.drawString(String.valueOf(pieniadze), 950, 20);
		g2d.drawImage(score, 910 ,25 , this);
		g2d.drawString(String.valueOf(punkty), 950, 45);
		

		//
	}
	
	public void zapiszGre(File file){
		
		try {
			FileOutputStream fos = new FileOutputStream(file);
			ObjectOutputStream oos = new ObjectOutputStream(fos);
			oos.writeObject(this);
			oos.close();
			fos.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	public byte[] zapiszGre(){
		byte [] zapisGry = null;
		try {
			ByteArrayOutputStream baos = new ByteArrayOutputStream();
			ObjectOutputStream oos = new ObjectOutputStream(baos);
			oos.writeObject(this);
			zapisGry = baos.toByteArray();
			oos.close();
			baos.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return zapisGry;
	}
	
	private void butonek(Graphics g, int x, int y, String tekst, Color color){
		Graphics2D g2d = (Graphics2D) g;
		g2d.setColor(color);
        g2d.setFont(FontGra);
        g2d.fillRect(x, y, 200, 20);
        g2d.setColor(Color.black);
        g2d.drawString(tekst, x + 10, y + 16);
	}
 
	@Override
	public void mousePressed(MouseEvent e) {
		
		
	}
	@Override
	public void mouseReleased(MouseEvent e) {
		if(stanDAD == true && pieniadze - 100 >= 0 && myszx < 850 && myszy < 500 && myszx > 0 && myszy > 0 && TabMap[dyskretneY][dyskretneX] != 2 && TabMap[dyskretneY][dyskretneX] != 3){
			wiezyczki.add(new Wiezyczka(dyskretneX,dyskretneY, jakityp));
			TabMap[dyskretneY][dyskretneX] = 3;
			pieniadze -= 100;
		}
		stanDAD = false;
		
	}
	@Override
	public void mouseEntered(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}
	@Override
	public void mouseExited(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}
	
	private void addZapis(byte[] zapis) {
		String kwerenda;
		try{
			Connection con = null;
			Class.forName("com.mysql.jdbc.Driver");
			con = DriverManager.getConnection( "jdbc:mysql://localhost/towerdefence", "root", "");
			kwerenda = "INSERT INTO zapisy(zapis) VALUES(?)";
			PreparedStatement ps = con.prepareStatement(kwerenda, Statement.RETURN_GENERATED_KEYS);
	        ps.setBytes(1, zapis);
	        ps.executeUpdate();
	        int id_zapisu = 0;
	        try (ResultSet generatedKeys = ps.getGeneratedKeys()) {
	            if (generatedKeys.next()) {
	                id_zapisu = generatedKeys.getInt(1);
	            }
	        }

	        kwerenda = "INSERT INTO zapisane_gry(id_gracza,id_zapisu,nazwa_zapisu) VALUES(?,?,?)";
			ps = con.prepareStatement(kwerenda);
	        ps.setInt(1, id);
	        ps.setInt(2, id_zapisu);
	        ps.setString(3, JOptionPane.showInputDialog("Podaj nazwe zapisu"));
	        ps.executeUpdate();
	        
	        
	        
	        con.close(); 
		}catch(Exception e){
			
			e.printStackTrace();
		}
		
	}
	
	@Override
	public void actionPerformed(ActionEvent e) {
		Object zrodlo = e.getSource();
		if (zrodlo == bStart){
			if (zegar != 0)
				pieniadze += 250;
			zegar = 0;
		}
		else if(zrodlo == bZapisz){
			timer.stop();

			Object[] possibleValues = { "Na dysk", "Do bazy"};
			Object selectedValue = JOptionPane.showInputDialog(null, "Wybierz metode", "Input", JOptionPane.INFORMATION_MESSAGE, null, possibleValues, possibleValues[0]);
			if(selectedValue == "Na dysk"){
				if(nowa == true){
					
					int wybor = fc.showSaveDialog(this);
					if (wybor == JFileChooser.APPROVE_OPTION) {
			            File file = fc.getSelectedFile();
			            if(!fc.getSelectedFile().getAbsolutePath().endsWith(".save")){
			                file = new File(fc.getSelectedFile() + ".save");
			            }
			            zapiszGre(file);
			        }
					
					//zapiszGre();
					//addZapis(zapiszGre());
				}
			}else if(selectedValue == "Do bazy"){
				addZapis(zapiszGre());
			}
			timer.start();
		}
		else if(zrodlo == bOtworz){
			timer.stop();
			int wybor = fc.showOpenDialog(this);
			if (wybor == JFileChooser.APPROVE_OPTION) {
	            File file = fc.getSelectedFile();
	            wczytajGre(file);
	        }
			timer.start();
		}
		
		boolean zieloneSwiatlo = true;
		Iterator <Potworek> i1 = potworki.iterator();
		while(i1.hasNext()){
			Potworek m = (Potworek) i1.next();
			if(TabMap[m.getY()/50][m.getX()/50] == 4 || TabMap[(m.getY() + 49)/50][(m.getX() + 49)/50] == 4)
				zieloneSwiatlo = false;
		}
		if(zegar == 0 && zieloneSwiatlo == true && !stosikPrzeciwnikow.empty()){
			potworki.add(new Potworek(Potworek.TypPotworka.valueOf(stosikPrzeciwnikow.pop())));
		}
		
		Iterator <Wiezyczka> w = wiezyczki.iterator();
		while(w.hasNext()){
			Wiezyczka m = (Wiezyczka) w.next();
			m.WarunekZniszczenia(potworki, punktyLiniAtaku);
		}
		Iterator <Potworek> i2 = potworki.iterator();
		while(i2.hasNext()){
			Potworek m = (Potworek) i2.next();

			if(m.move(TabMap))
				serduszka--;
			if(m.getZycie() < 0){
				i2.remove();
				pieniadze += 50;
				punkty+= 100;
			}
			
		}
		if(zegar > 0)
			zegar--;
		else if(stosikPrzeciwnikow.empty()){
			try {
				wczytajPrzeciwnikow(2);
			} catch (IOException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			zegar = 1200;
		}
		if(serduszka < 0)
			winCondition();
		repaint();
	}
	private void winCondition() {
		
		String name = JOptionPane.showInputDialog("Podaj nazwe");

			PrintWriter pw = null;
			try {
				pw = new PrintWriter(new FileWriter("ranking.txt", true));
				pw.println(name + ',' + punkty);
				pw.close();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			finally{
				pw.close();
			}
			

		System.exit(0);
	}
	private void wczytajGre(File file) {
		PanelGra temp = null;
		try {
			FileInputStream fis = new FileInputStream(file);
			ObjectInputStream ois = new ObjectInputStream(fis);
			temp = (PanelGra) ois.readObject();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		this.TabMap = temp.TabMap;
		this.wiezyczki = temp.wiezyczki;
		this.potworki = temp.potworki;
		this.punktyLiniAtaku = temp.punktyLiniAtaku;
		this.stosikPrzeciwnikow = temp.stosikPrzeciwnikow;
		this.zegar = temp.zegar;
		this.pieniadze = temp.pieniadze;
		this.serduszka = temp.serduszka;
		this.punkty = temp.punkty;
		this.przeciwnicy = temp.przeciwnicy;
	}
	public void wczytajGre(byte[] zapisDane) {
		PanelGra temp = null;
		try {
			ByteArrayInputStream bais = new ByteArrayInputStream(zapisDane);
			ObjectInputStream ois = new ObjectInputStream(bais);
			temp = (PanelGra) ois.readObject();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		this.TabMap = temp.TabMap;
		this.wiezyczki = temp.wiezyczki;
		this.potworki = temp.potworki;
		this.punktyLiniAtaku = temp.punktyLiniAtaku;
		this.stosikPrzeciwnikow = temp.stosikPrzeciwnikow;
		this.zegar = temp.zegar;
		this.pieniadze = temp.pieniadze;
		this.serduszka = temp.serduszka;
		this.punkty = temp.punkty;
		this.przeciwnicy = temp.przeciwnicy;
	}
}
