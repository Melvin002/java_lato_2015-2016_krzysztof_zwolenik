package SP;

import java.io.File;

import javax.swing.filechooser.FileFilter;


public class MapFileFilter extends FileFilter{

	@Override
	public boolean accept(File f) {
		if (f.isDirectory()) {
            return true;
        }
        if (f.getAbsolutePath().endsWith(".map")){
                return true;
        } else {
            return false;
        }
	}

	@Override
	public String getDescription() {
		return ".map, pliki z mapa";
	}

}

