package SP;

import java.awt.Color;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;
import java.sql.*;

import javax.swing.DefaultListModel;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

import TD.OknoGry;

public class PanelGraJednoosobowa extends JPanel implements ActionListener, ListSelectionListener{

	OknoGry oG;
	int id_gracza;
	JButton bLogowanie, bRejestracja, bNowaGra, bZaloguj, bZarejestruj, bWczytajGre;
	JTextField tLogin, tHaslo, tReHaslo;
	JLabel lLogin, lHaslo, lReHaslo, lStatus;
	String pass = "123";
	String log = "elo";
	JList<PozycjaListy> list;
	DefaultListModel<PozycjaListy> model;
	Connection con = null;
	final JFileChooser fc = new JFileChooser();

	
	public PanelGraJednoosobowa(OknoGry oknoGry){
		super();
		fc.addChoosableFileFilter(new MapFileFilter() );
		this.oG = oknoGry;
		JLabel background = new JLabel(new ImageIcon("tloo.jpg"));
		background.setBounds(0, 0, 853, 480);
		setVisible(false);	
		setLayout(null);
		setBounds(0, 0, 853, 480);
		
		bLogowanie = dodajPrzycisk("LOGOWANIE", 0); 
		bRejestracja = dodajPrzycisk("REJESTRACJA", 1); 
		bNowaGra = dodajPrzycisk("NOWA GRA", 2); 
		bNowaGra.setEnabled(false);
		bWczytajGre = dodajPrzycisk("WCZYTAJ GRE", 3); 
		bWczytajGre.setEnabled(false);
		
		tLogin = new JTextField(15);
	    tHaslo = new JPasswordField(15);
	    lLogin = new JLabel("Nazwa u�ytkownika");
	    lHaslo = new JLabel("Has�o");
	    tReHaslo = new JPasswordField(15);
	    lReHaslo = new JLabel("Powt�rz has�o");
	    
	    lLogin.setVisible(false);
	    tLogin.setVisible(false);
	    lHaslo.setVisible(false);
	    tHaslo.setVisible(false);
	    tReHaslo.setVisible(false);
	    lReHaslo.setVisible(false);
	    
	    lLogin.setBounds(550,100,120,20);
	    tLogin.setBounds(550,130,150,20);
	    lHaslo.setBounds(550,160,150,20);
	    tHaslo.setBounds(550,190,150,20);
	    lReHaslo.setBounds(550,220,150,20);
	    tReHaslo.setBounds(550,250,150,20);
	    
	    this.add(tLogin);
	    this.add(tHaslo);
	    this.add(lLogin);
	    this.add(lHaslo);
	    this.add(tReHaslo);
	    this.add(lReHaslo);
	    
	    bZaloguj = dodajPrzycisk("ZALOGUJ",560,230, 130, 30);
		bZaloguj.setVisible(false);
		
		bZarejestruj = dodajPrzycisk("ZAREJESTRUJ",530,290, 190, 30);
		bZarejestruj.setVisible(false);
		
		lStatus = new JLabel("Niezalogowany");
		lStatus.setVisible(true);
		lStatus.setBounds(750,3,100,20);
		
		this.add(lStatus);
		
		model = new DefaultListModel<PozycjaListy>();
		list = new JList<PozycjaListy>(model);
		list.addListSelectionListener(this);
		

		JScrollPane pane = new JScrollPane(list);
		pane.setBounds(120, 260, 230, 60);
		this.add(pane);
		
		this.add(background);
	}
	private void login(){
		lReHaslo.setVisible(false);
	    tReHaslo.setVisible(false);
	    lLogin.setVisible(true);
	    tLogin.setVisible(true);
	    lHaslo.setVisible(true);
	    tHaslo.setVisible(true);
	    bZaloguj.setVisible(true);
	    bZarejestruj.setVisible(false);
	}
	private void addUser(String login, String haslo) {
		String kwerenda;
		
		try{
			Class.forName("com.mysql.jdbc.Driver");
			con = DriverManager.getConnection( "jdbc:mysql://localhost/towerdefence", "root", "");
			kwerenda = "INSERT INTO gracze(nazwa,haslo) VALUES(?,?)";
			PreparedStatement ps = con.prepareStatement(kwerenda);
	        ps.setString(1, login);
	        ps.setString(2, haslo);
	        ps.executeUpdate();
	        con.close();
		}catch(Exception e){
			
			e.printStackTrace();
		}
		
	}
	
	private void rejestracja(){
	    bZaloguj.setVisible(false);
	    lLogin.setVisible(true);
	    tLogin.setVisible(true);
	    lHaslo.setVisible(true);
	    tHaslo.setVisible(true);
		lReHaslo.setVisible(true);
	    tReHaslo.setVisible(true);
	    bZarejestruj.setVisible(true);
	}
	
	private JButton dodajPrzycisk(String nazwa, int pozycja){
		JButton button;
		button = new JButton(nazwa);
		button.setBackground(Color.DARK_GRAY);
		button.setForeground(Color.WHITE);
		button.setBounds(100, 60 + pozycja * 50, 270, 40);
		button.setFocusPainted(false);
		button.setFont(new Font("Arial", Font.BOLD, 20));
		button.addActionListener(this);
		this.add(button);
		return button;
	}
	
	private boolean loginChck(String login, String haslo){
		String kwerenda;
		boolean zalogowany = false;
		
		try{
			Class.forName("com.mysql.jdbc.Driver");
			con = DriverManager.getConnection( "jdbc:mysql://localhost/towerdefence", "root", "");
			kwerenda = "SELECT nazwa, haslo, id_gracza FROM gracze WHERE nazwa = ?";
			PreparedStatement ps = con.prepareStatement(kwerenda);
	        ps.setString(1, login);
	        ResultSet rs = ps.executeQuery();
	        
	        String chckLogin = null;
			String chckHaslo = null;
			
	        while (rs.next()) {

				chckLogin = rs.getString("nazwa");
				chckHaslo = rs.getString("haslo");
				id_gracza = rs.getInt("id_gracza");
			}
	        if(chckLogin != null && (chckLogin.equals(login)) && (chckHaslo.equals(haslo))){
	            zalogowany = true;
	        }

	        con.close();
		}catch(Exception e){
			e.printStackTrace();
		}
		return zalogowany;
	}
	
	private boolean userChck(String login){
		String kwerenda;
		boolean jestjuztaki = false;
		
		try{
			Class.forName("com.mysql.jdbc.Driver");
			con = DriverManager.getConnection( "jdbc:mysql://localhost/towerdefence", "root", "");
			kwerenda = "SELECT nazwa FROM gracze WHERE nazwa = ?";
			PreparedStatement ps = con.prepareStatement(kwerenda);
	        ps.setString(1, login);
	        ResultSet rs = ps.executeQuery();
	        
	        String chckLogin = null;
			
	        while (rs.next()) {
				chckLogin = rs.getString("nazwa");
			}
	        if(chckLogin != null && (chckLogin.equals(login))){
	        	jestjuztaki = true;
	        }

	        con.close();
		}catch(Exception e){
			e.printStackTrace();
		}
		return jestjuztaki;
	}
	
	private JButton dodajPrzycisk(String nazwa, int x, int y, int width, int height){
		JButton button;
		button = new JButton(nazwa);
		button.setBackground(Color.DARK_GRAY);
		button.setForeground(Color.WHITE);
		button.setBounds(x, y, width, height);
		button.setFocusPainted(false);
		button.setFont(new Font("Arial", Font.BOLD, 20));
		button.addActionListener(this);
		this.add(button);
		return button;
	}
	@Override
	public void actionPerformed(ActionEvent e) {
		Object zrodlo = e.getSource();
		if (zrodlo == bLogowanie){
			login();
		}
		else if(zrodlo == bRejestracja){
			rejestracja();
		}
		else if(zrodlo == bZaloguj){
			String login_inp = tLogin.getText().trim();
			String haslo_inp = tHaslo.getText().trim();

			if(login_inp.equals("") || haslo_inp.equals("")){
				JOptionPane.showMessageDialog(null,"Prosz� poda� login i has�o");
			}
			else if(loginChck(login_inp, haslo_inp)) {
				JOptionPane.showMessageDialog(null,"Pomy�lnie zalogowano");
				bNowaGra.setEnabled(true);
				lStatus.setText(login_inp);
				fillList();
				
	        }
	        else {
				JOptionPane.showMessageDialog(null,"Z�a nazwa u�yszkodnika lub has�o");
				tLogin.setText("");
				tHaslo.setText("");
				tLogin.requestFocus();
	        }
		}
		else if(zrodlo == bZarejestruj){
			String login = tLogin.getText().trim();
			String haslo = tHaslo.getText().trim();
			String Rehaslo = tReHaslo.getText().trim();
			
			if(!haslo.equals(Rehaslo)){
				JOptionPane.showMessageDialog(null,"Niepoprawnie powt�rzy�e� has�o");
				tHaslo.setText("");
				tHaslo.requestFocus();
				tReHaslo.setText("");
			}else
			if(userChck(login)){
				JOptionPane.showMessageDialog(null,"Nazwa u�ytkownika zaj�ta");
				tLogin.setText("");
				tLogin.requestFocus();
			}else{
				addUser(login, haslo);
				JOptionPane.showMessageDialog(null,"Pomyslnie zarejestrowano, prosze sie zalogowac");
			}
			
		}
		else if(zrodlo == bNowaGra){
			File file = null;
			if (fc.showOpenDialog(this) == JFileChooser.APPROVE_OPTION) {
	            file = fc.getSelectedFile();
	        }
			String fileS = file.getAbsolutePath();
			if(!fileS.endsWith(".map")){
				JOptionPane.showMessageDialog(this,"Wybra�e� z�y plik, mapy maja rozszerzenie .map");
				System.exit(0);
			}
			this.setVisible(false);
			oG.sp(id_gracza,fileS);
		}
		else if(zrodlo == bWczytajGre){
			
			oG.sp(id_gracza, null);
			oG.panel_gra.wczytajGre(getZapis(list.getSelectedValue().id));
			this.setVisible(false);
		}
	}
	private byte [] getZapis(int idZap) {
		Blob dane = null;
		try{
			Class.forName("com.mysql.jdbc.Driver");
			con = DriverManager.getConnection( "jdbc:mysql://localhost/towerdefence", "root", "");
			String kwerenda = "SELECT zapis FROM zapisy WHERE id_zapisu = ?";
			PreparedStatement ps = con.prepareStatement(kwerenda);
	        ps.setInt(1, idZap);
	        ResultSet rs = ps.executeQuery();
	        while (rs.next())
	        	dane = rs.getBlob("zapis");
	        con.close();
		}catch(Exception e){
			e.printStackTrace();
		}
		byte[] dane_b = null;
		try {
			dane_b = dane.getBytes(1, (int) dane.length());
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return dane_b;	
	}
	private void fillList() {
		try{
			Class.forName("com.mysql.jdbc.Driver");
			con = DriverManager.getConnection( "jdbc:mysql://localhost/towerdefence", "root", "");
			String kwerenda = "SELECT Nazwa_zapisu, Data, id_zapisu FROM zapisane_gry WHERE id_gracza = ?";
			PreparedStatement ps = con.prepareStatement(kwerenda);
	        ps.setInt(1, id_gracza);
	        ResultSet rs = ps.executeQuery();
	        while (rs.next()) {

				String sNazwa = rs.getString("Nazwa_zapisu");
				String sData = rs.getString("Data");
				int id_zapisu = rs.getInt("id_zapisu");
				model.addElement(new PozycjaListy((sNazwa + " \t"  + sData),id_zapisu));
			}

	        con.close();
		}catch(Exception e){
			e.printStackTrace();
		}
		
	}
	public void valueChanged(ListSelectionEvent e) {
	    if (e.getValueIsAdjusting() == false) {

	        if (list.getSelectedIndex() == -1) {
	        	bWczytajGre.setEnabled(false);

	        } else {
	        	bWczytajGre.setEnabled(true);
	        }
	        
	    }
	}
}
