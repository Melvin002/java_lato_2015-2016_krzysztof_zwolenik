package MP;

public class PakietObrazenia extends Pakiet {

	public int zycie, id;
	
	public PakietObrazenia(int id, int zycie) {
		super(Pakiet.OBRAZENIA);
		this.id = id;
		this.zycie = zycie;
	}
	public PakietObrazenia(byte [] dane) {
		super(Pakiet.OBRAZENIA);
		String [] informacje = odczytajInf(dane).split(",");
		this.id = Integer.parseInt(informacje[0]);
		this.zycie = Integer.parseInt(informacje[1]);
		
	}

	@Override
	public byte[] getDane() {
		
		return ("06" + id + "," + zycie).getBytes();
	}

	@Override
	public void wyslijPakiet(WatekUDP server) {
		server.wyslijDoWyszystkich(getDane());
	}

	@Override
	public void wyslijPakiet(KlientGry client) {
		client.wyslijPakiet(getDane());
		
	}
	
}
