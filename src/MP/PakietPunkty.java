package MP;

public class PakietPunkty extends Pakiet {

	public int pkt;
	
	public PakietPunkty(int pkt) {
		super(Pakiet.PKT);
		this.pkt = pkt;
	}
	public PakietPunkty(byte [] dane) {
		super(Pakiet.PKT);
		String [] informacje = odczytajInf(dane).split(",");
		this.pkt = Integer.parseInt(informacje[0]);
	}

	@Override
	public byte[] getDane() {
		
		return ("12" + pkt).getBytes();
	}

	@Override
	public void wyslijPakiet(WatekUDP server) {
		server.wyslijDoWyszystkich(getDane());
	}

	@Override
	public void wyslijPakiet(KlientGry client) {
		client.wyslijPakiet(getDane());
		
	}
	
}
