package MP;

public class PakietSerduszka extends Pakiet {

	public int serc;
	
	public PakietSerduszka(int czas) {
		super(Pakiet.SERDUSZKA);
		this.serc = czas;
	}
	public PakietSerduszka(byte [] dane) {
		super(Pakiet.SERDUSZKA);
		String [] informacje = odczytajInf(dane).split(",");
		this.serc = Integer.parseInt(informacje[0]);
	}

	@Override
	public byte[] getDane() {
		
		return ("11" + serc).getBytes();
	}

	@Override
	public void wyslijPakiet(WatekUDP server) {
		server.wyslijDoWyszystkich(getDane());
	}

	@Override
	public void wyslijPakiet(KlientGry client) {
		client.wyslijPakiet(getDane());
		
	}
	
}
