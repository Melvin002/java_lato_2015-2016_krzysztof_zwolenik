package MP;
import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Font;
import java.awt.FontFormatException;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.Point;
import java.awt.Stroke;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectInputStream;
import java.io.ObjectOutput;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Random;
import java.util.Stack;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;
import javax.swing.Timer;

import TD.Potworek;
import TD.Wiezyczka;
import TD.Wiezyczka.TrybStrzelania;
import TD.Wiezyczka.TypWierzyczki;

//setSize(867,540);
public class StronaClienta extends JPanel implements MouseMotionListener, MouseListener, ActionListener{

	
	public int [][] TabMap = new int[10][17]; //[y][x]
	Stack<String> stosikPrzeciwnikow;
	
	boolean spektator = false;
	Color klientColor;
	private boolean stanDAD = false;
	private int myszx, myszy, dyskretneX, dyskretneY;
	ArrayList<Wiezyczka> wiezyczki;
	ArrayList<Potworek> potworki;
	ArrayList<Punkt> pMalowanie;
	ArrayList<Point> punktyLiniAtaku;
	Wiezyczka.TypWierzyczki jakityp;
	Wiezyczka.TrybStrzelania trybStrzelania;
	Wiezyczka WiezyczkaWmenu = null;
	Font FontGra;
	String nick;
	
	public Object lock3 = new Object();
	public Object lock2 = new Object();
	public Object lock1 = new Object();
	
	int licznikiteracji = 1; //wyswitlanie promieni lasera
	int zegar = 0; // czas w sekundach= wart/40
	private Timer timer;
	int pieniadze, serduszka, punkty;
	Image image1, image2, image3, serce, clock, dollar, score;
	JButton bStart;
	ArrayList<String> przeciwnicy;
	KlientGry client = null;
	
	public StronaClienta(KlientGry klientGry)  throws IOException{
		
		client = klientGry;
		try {
			//InputStream in = getClass().getResourceAsStream("res/FontGra.ttf");
			FontGra = Font.createFont(Font.TRUETYPE_FONT, new File("FontGra.ttf")).deriveFont(20f);
			
		} catch (FontFormatException e) {
			e.printStackTrace();
		}
		Random generator = new Random(); 
		
		klientColor = new Color(generator.nextInt(255),generator.nextInt(255),generator.nextInt(255),255);
		jakityp = Wiezyczka.TypWierzyczki.DZIALO;
		setVisible(false);	
		setLayout(null);
		addMouseMotionListener(this);
		addMouseListener(this);
		wiezyczki = new ArrayList<Wiezyczka>();
		potworki = new ArrayList<Potworek>();
		punktyLiniAtaku = new ArrayList<Point>();
		pMalowanie = new ArrayList<Punkt>();
		stosikPrzeciwnikow = new Stack<String>();
		//
		bStart = new JButton("START");
		bStart.setBackground(Color.DARK_GRAY);
		bStart.setForeground(Color.WHITE);
		bStart.setBounds(1000, 25, 80, 20);
		bStart.setFocusPainted(false);
		bStart.setFont(FontGra);
		bStart.addActionListener(this);
		this.add(bStart);
		//
        timer = new Timer(25, this);
        timer.start();
        //
        ImageIcon ii1 = new ImageIcon("wieza1.jpg");
		image1 = ii1.getImage();
		ii1 = new ImageIcon("wieza2.jpg");
		image2 = ii1.getImage();
		ii1 = new ImageIcon("wieza3.jpg");
		image3 = ii1.getImage();
        //
		ii1 = new ImageIcon("clock.png");
		clock = ii1.getImage();
		ii1 = new ImageIcon("heart.png");
		serce = ii1.getImage();
		ii1 = new ImageIcon("dollar.png");
		dollar = ii1.getImage();
		ii1 = new ImageIcon("score.png");
		score = ii1.getImage();

		
	}
    @Override
    public void paintComponent(Graphics g) {
        super.paintComponent(g);
        
        
        wyrysujMape(g);
        boczneMenuIkony(g);
        if(spektator == false)
        	boczneMenuRysuj(g);
        rysujWiezyczkiPotworki(g);
        zasiegWiez(g);
        if(stanDAD == true){
        	rysujWidmoWiezyczki(g, jakityp, myszx, myszy);
        	repaint();
        }
        synchronized (lock2) {
        	rysujMalunki(g);
		}
        	
        atak(g);
    }
    private void atak(Graphics g){
    	synchronized(lock3){
			Iterator <Point> punkt = punktyLiniAtaku.iterator();
			while(punkt.hasNext()){
				licznikiteracji++;
				Point p = (Point) punkt.next();
				int tmpx = p.x + 17;
				int tmpy = p.y + 17;
				if(licznikiteracji % 20 == 0)
					punkt.remove();
				p = (Point) punkt.next();
				liniaAtaku(g, tmpx, tmpy, p.x + 25, p.y + 25);
				if(licznikiteracji % 20 == 0){
					punkt.remove();
					licznikiteracji=0;
				}
			}
    	}
    }
    public void rysujMalunki(Graphics g){
    	
		Graphics2D g2d = (Graphics2D) g;
		g2d.setStroke(new BasicStroke(6));
		Iterator <Punkt> i = pMalowanie.iterator();
		Punkt p = null;
		if(i.hasNext()){
			p = (Punkt) i.next();
			if(p.alfa <= 0)
			i.remove();
		}
		while(i.hasNext()){		
			Punkt p1 = (Punkt) i.next();
			g2d.setColor(p.decay());
			g2d.drawLine(p.x,p.y,p1.x,p1.y);
			//p.decay();
			
			p = p1;
			
		}					
    }
    public void liniaAtaku(Graphics g, int x1, int y1, int x2, int y2){
    	Graphics2D g2d = (Graphics2D) g;
    	
    	g2d.setStroke(new BasicStroke(10));
    	g2d.setColor(new Color(254, 73, 2, 180));
    	g2d.drawLine(x1, y1, x2, y2);
    }
    private void rysujWidmoWiezyczki(Graphics g, Wiezyczka.TypWierzyczki jakityp, int x, int y){
    	Image image = null;
    	switch (jakityp){
    	case DZIALO:
    		image = new ImageIcon("wieza1.jpg").getImage();
    		break;
    	case KARABIN:
    		image = new ImageIcon("wieza2.jpg").getImage();
    		break;

    	case SPOWALNIACZ:
    		image = new ImageIcon("wieza3.jpg").getImage();
    		break;
    	}
    	Graphics2D g2d = (Graphics2D) g;
    	g2d.drawImage(image, x - 20,y - 20, this);
    }
    private void rysujWiezyczkiPotworki(Graphics g) {
		Graphics2D g2d = (Graphics2D) g;
		for(Wiezyczka m:wiezyczki){
			g2d.drawImage(m.getImage(), m.getX() ,m.getY() , this);
		}
		synchronized(lock1){
			for(Potworek m:potworki){
				m.rysujPotworka(g2d, this);
				repaint();
			}
		}
		
	}
	private void zasiegWiez(Graphics g){
		
		int tempx = myszx/50;
		int tempy = myszy/50;
		Graphics2D g2d = (Graphics2D) g;
		Iterator <Wiezyczka> i = wiezyczki.iterator();
		while(i.hasNext()){
			Wiezyczka m = (Wiezyczka) i.next();
			if(tempx == m.getX()/50 && tempy == m.getY()/50){
				g2d.setColor(new Color(255, 153, 0, 120));	
				g2d.fillOval(tempx * 50 - (m.getZasieg() * 2-50)/2, tempy * 50 - (m.getZasieg() * 2-50)/2, m.getZasieg()*2, m.getZasieg()*2);					
			}
		}		
	}
	private void podswietlanieMapy(Graphics2D g2d, Color color, int i,int j){
		g2d.setColor(color);					
		g2d.fillRect(j*50, i *50, 50, 50);
		g2d.setColor(Color.BLACK);
		int thickness = 1;
		Stroke oldStroke = g2d.getStroke();
		g2d.setStroke(new BasicStroke(thickness));
		g2d.drawRect(j*50,i *50, 50, 50);
		g2d.setStroke(oldStroke);
	}
	private void wyrysujMape(Graphics g){	
		
		Graphics2D g2d = (Graphics2D) g;
		for(int i = 0; i<10; i++){
			for(int j = 0; j<17; j++){
				if(TabMap[i][j] == 2 || TabMap[i][j] == 4 || TabMap[i][j] == 5){
					podswietlanieMapy(g2d, Color.decode("#A0785A"), i, j);

				}else{ 
					podswietlanieMapy(g2d, Color.decode("#77DD77"), i, j);
				}
			}	
		}
		int dyskretneX = myszx/50, dyskretneY=myszy/50;
		if(dyskretneX < 17 && dyskretneY < 10 && dyskretneX >= 0 && dyskretneY >= 0){
			if(TabMap[dyskretneY][dyskretneX] == 2){
				podswietlanieMapy(g2d, Color.decode("#ff4444"), dyskretneY, dyskretneX);

			}else{ 
				podswietlanieMapy(g2d, Color.decode("#81d8d0"), dyskretneY, dyskretneX);
			}
			
		}

	}
	@Override
	public void mouseDragged(MouseEvent e) {
		myszx = e.getX();
		myszy = e.getY();
		dyskretneX = myszx/50;
		dyskretneY = myszy/50;
		if(spektator == false){
			if(stanDAD == false && myszx > 884 && myszx < 919 && myszy > 50 && myszy < 100){
				stanDAD = true;
				jakityp = Wiezyczka.TypWierzyczki.DZIALO;
			}
			else if(stanDAD == false && myszx > 983 && myszx < 1019 && myszy > 50 && myszy < 100){
				stanDAD = true;
				jakityp = Wiezyczka.TypWierzyczki.KARABIN;
			}
			else if(stanDAD == false && myszx > 1084 && myszx < 1119 && myszy > 50 && myszy < 100){
				stanDAD = true;
				jakityp = Wiezyczka.TypWierzyczki.SPOWALNIACZ;
			}
		}
		else{
			if(pMalowanie.size() < 200){
				synchronized (lock2) {
					Punkt test = new Punkt(myszx,myszy, klientColor);
					PakietMalowanie pakiet = new PakietMalowanie(test);
					pakiet.wyslijPakiet(client);
				}
			}
			else if(pMalowanie.size() == 200){
				mouseReleased(e);
			}
		}
		
	}
	@Override
	public void mouseMoved(MouseEvent e) {
		myszx = e.getX();
		myszy = e.getY();
		dyskretneX = myszx/50;
		dyskretneY = myszy/50;
		
	}
	@Override
	public void mouseClicked(MouseEvent e) {
		dyskretneX = myszx/50;
		dyskretneY = myszy/50;
		if(spektator == false){
    		Iterator <Wiezyczka> i = wiezyczki.iterator();
    		while(i.hasNext()){
    			Wiezyczka m = (Wiezyczka) i.next();
    			if(SwingUtilities.isRightMouseButton(e) && dyskretneX == m.getX()/50 && dyskretneY == m.getY()/50){
    				
    				PakietKasujWiezyczke pakiet = new PakietKasujWiezyczke(dyskretneX,dyskretneY,m.getTypWierzyczki());
    				pakiet.wyslijPakiet(client);
    			}
    		}
		}   

	}
	private void boczneMenuIkony(Graphics g){
		
		Graphics2D g2d = (Graphics2D) g;
		g2d.setColor(Color.BLACK);
		g2d.fillRect(851, 0, 300, 501);
		
		g2d.setColor(Color.BLACK);
		g2d.setColor(Color.white);
		g2d.setFont(FontGra);
		g2d.drawImage(clock, 850 ,0 , this);
		g2d.drawString(String.valueOf(zegar/40), 880, 20);	
		g2d.drawImage(serce, 850 ,25 , this);
		g2d.drawString(String.valueOf(serduszka), 880, 45);
		g2d.drawImage(dollar, 910 ,0 , this);
		g2d.drawString(String.valueOf(pieniadze), 950, 20);
		g2d.drawImage(score, 910 ,25 , this);
		g2d.drawString(String.valueOf(punkty), 950, 45);
		if(nick != null)
			g2d.drawString(nick, 1000, 20);
	}
	private void boczneMenuRysuj(Graphics g){
		//
		Graphics2D g2d = (Graphics2D) g;
		g2d.setColor(Color.BLACK);
		
		g2d.drawImage(image1, 884 ,50 , this);
		g2d.drawImage(image2, 983 ,50 , this);
		g2d.drawImage(image3, 1084 ,50 , this);
		//wiezyczki

	}
 
	@Override
	public void mousePressed(MouseEvent e) {
		
		
	}
	@Override
	public void mouseReleased(MouseEvent e) {
		if(spektator == false){
			if(stanDAD == true && pieniadze - 100 >= 0 && myszx < 850 && myszy < 500 && myszx > 0 && myszy > 0 && TabMap[dyskretneY][dyskretneX] != 2 && TabMap[dyskretneY][dyskretneX] != 3){
				
				PakietDodajWiezyczke pakiet = new PakietDodajWiezyczke(dyskretneX,dyskretneY,jakityp);
				pakiet.wyslijPakiet(client);
				TabMap[dyskretneY][dyskretneX] = 3;
			}
			stanDAD = false;	
		} else {
			synchronized (lock2) {
				Punkt test = new Punkt(myszx,myszy, new Color(0,0,0,0));
				PakietMalowanie pakiet = new PakietMalowanie(test);
				pakiet.wyslijPakiet(client);
			}
		}
	}
	@Override
	public void mouseEntered(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}
	@Override
	public void mouseExited(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}
	@Override
	public void actionPerformed(ActionEvent e) {
		Object zrodlo = e.getSource();
		if (zrodlo == bStart && zegar != 0){
			PakietZegar pakiet = new PakietZegar(0);
			pakiet.wyslijPakiet(client);
		}
		synchronized(lock1){
			Iterator <Potworek> i2 = potworki.iterator();
			while(i2.hasNext()){
				Potworek m = (Potworek) i2.next();
				if(m.move(TabMap))
					serduszka--;
				if(m.getZycie() < 0){
					i2.remove();
				}			
			}
		}
		if(serduszka < 0){
			int result = JOptionPane.showConfirmDialog(this, ("Wasz wynik to:" + punkty), "Gratulacje",  JOptionPane.DEFAULT_OPTION);
			if (result == JOptionPane.OK_OPTION || result == JOptionPane.CLOSED_OPTION)
				System.exit(0);
		}
		repaint();
	}
	public void zegarStart(int czas){
		zegar = czas;
	}
	
}
