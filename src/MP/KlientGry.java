package MP;

import java.awt.Color;
import java.awt.Point;
import java.io.DataInputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.Socket;
import java.net.SocketException;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.Iterator;

import javax.swing.JOptionPane;

import TD.Potworek;
import TD.Wiezyczka;

public class KlientGry extends Thread{
	
	private String hostName = "localhost";
	private InetAddress address = null;
	private int port = 1337;
	private int portUDP = 1338;
	public StronaClienta stronaClienta = null;
	private DatagramSocket socket;
	
	public KlientGry(){
		try {
			stronaClienta = new StronaClienta(this);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			socket.close();
			e.printStackTrace();
		}
		this.start();
		
	}
	private void analizaPakietu(byte[] dane, InetAddress address, int port) {
		String informacja = new String(dane).trim();
		int typPakietu = Integer.parseInt(informacja.substring(0,2));
		Pakiet pakiet = null;
		switch(typPakietu){
		case Pakiet.LOGIN:
			pakiet = new PakietLogin(dane,true);
			PakietLogin p0 = (PakietLogin) pakiet;
			if(p0.index >= 2)
				stronaClienta.spektator = true;
			break;
			
		case Pakiet.ZEGAR:
			pakiet = new PakietZegar(dane);
			PakietZegar p1 = (PakietZegar) pakiet;
			stronaClienta.zegarStart(p1.czas);
			break;
		
		case Pakiet.DODAJWIEZ:
			pakiet = new PakietDodajWiezyczke(dane);
			PakietDodajWiezyczke p2 = (PakietDodajWiezyczke) pakiet;
			stronaClienta.wiezyczki.add(new Wiezyczka(p2.x,p2.y,p2.typ, p2.id));
			break;
			
		case Pakiet.USUNWIEZ:
			pakiet = new PakietKasujWiezyczke(dane);
			PakietKasujWiezyczke p3 = (PakietKasujWiezyczke) pakiet;
			Wiezyczka w = new Wiezyczka(p3.x, p3.y, p3.typ);
			stronaClienta.wiezyczki.remove(w);
			stronaClienta.TabMap[p3.y][p3.x] = 1;
			//System.out.println(this);
			break;
		case Pakiet.DODAJPOT:
			
			pakiet = new PakietDodajPotworka(dane);
			PakietDodajPotworka p4 = (PakietDodajPotworka) pakiet;
			Potworek p = new Potworek(p4.p.getTyp());
			p = p4.p;
			synchronized(stronaClienta.lock1){
				stronaClienta.potworki.add(p);
			}
			break;
		case Pakiet.MALUNKI:
			pakiet = new PakietMalowanie(dane);
			PakietMalowanie p5 = (PakietMalowanie) pakiet;
			Punkt pkt = new Punkt(p5.x, p5.y, new Color(p5.r, p5.g, p5.b, p5.alfa/5));
			synchronized (stronaClienta.lock2) {
				stronaClienta.pMalowanie.add(pkt);
			}
			break;
		case Pakiet.OBRAZENIA:
			pakiet = new PakietObrazenia(dane);
			PakietObrazenia p6 = (PakietObrazenia) pakiet;
			Iterator <Potworek> i1 = stronaClienta.potworki.iterator();
			synchronized(stronaClienta.lock1){
				while(i1.hasNext()){
					Potworek m = (Potworek) i1.next();
					if(p6.id == m.id){
						m.setZycie(p6.zycie);
						break;
					}
				}
			}
			break;
		case Pakiet.LINIEATK:
			pakiet = new PakietPrzeslijLinie(dane);
			PakietPrzeslijLinie p7 = (PakietPrzeslijLinie) pakiet;
			synchronized(stronaClienta.lock3){
				stronaClienta.punktyLiniAtaku = p7.p;
				stronaClienta.repaint();
			}
			break;
		case Pakiet.KASA:
			pakiet = new PakietKasa(dane);
			PakietKasa p8 = (PakietKasa) pakiet;
			stronaClienta.pieniadze = p8.kasa;
			break;
		case Pakiet.SERDUSZKA:
			pakiet = new PakietSerduszka(dane);
			PakietSerduszka p9 = (PakietSerduszka) pakiet;
			stronaClienta.serduszka = p9.serc;
			break;
		case Pakiet.PKT:
			pakiet = new PakietPunkty(dane);
			PakietPunkty p10 = (PakietPunkty) pakiet;
			stronaClienta.punkty = p10.pkt;
			break;
		case Pakiet.DODAJPOT2:
			pakiet = new PakietDodajPotworka2(dane);
			PakietDodajPotworka2 p11 = (PakietDodajPotworka2) pakiet;
			Potworek p_ = new Potworek(p11.p.getTyp());
			p_ = p11.p;
			synchronized(stronaClienta.lock1){
				stronaClienta.potworki.add(p_);
			}
			System.out.println("dodajemy potwa");
			break;
		case Pakiet.DODAJWIEZ2:
			pakiet = new PakietDodajWiezyczke(dane);
			PakietDodajWiezyczke p12 = (PakietDodajWiezyczke) pakiet;
			stronaClienta.wiezyczki.add(new Wiezyczka(p12.x,p12.y,p12.typ, p12.id));
			System.out.println("dodajemy wieze");
			break;
		}

	}
	public void wyslijPakiet(byte [] dane){
		DatagramPacket packet = new DatagramPacket(dane, dane.length, address, portUDP);
		try {
            socket.send(packet);
        } catch (IOException e) {
            e.printStackTrace();
        }
	}
	public void run(){
		try {
			address = InetAddress.getLocalHost();
		} catch (UnknownHostException e1) {
				
			e1.printStackTrace();
		}
		try ( 
			Socket socketTCP = new Socket(hostName, port);
			ObjectInputStream in = new ObjectInputStream(socketTCP.getInputStream());
			
		){
			
			for(int i = 0; i<10; i++){
				for(int j = 0; j<17; j++){
					stronaClienta.TabMap[i][j] = in.readInt();
				}
			}	
			try {
				stronaClienta.przeciwnicy = (ArrayList<String>) in.readObject();
			} catch (ClassNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		} catch (UnknownHostException e) {
			e.printStackTrace();
		} catch (IOException e) {
			JOptionPane.showMessageDialog(null, "Nie uda�o si� po��czyc z serwerem");
			System.exit(0);
			e.printStackTrace();
		}
		
		
		try {
			
			socket = new DatagramSocket();

			String nick = JOptionPane.showInputDialog("Podaj nazwe");
			PakietLogin pakietlogin = new PakietLogin(nick);
			stronaClienta.nick = nick;
			pakietlogin.wyslijPakiet(this);
			
			PakietKasa pakietKasa = new PakietKasa(666);
			pakietKasa.wyslijPakiet(this);
			
			PakietSerduszka pakietSerc = new PakietSerduszka(666);
			pakietSerc.wyslijPakiet(this);
			
			PakietSerduszka pakietPkt = new PakietSerduszka(666);
			pakietPkt.wyslijPakiet(this);
			
			PakietDodajPotworka2 pakietProszePotw = new PakietDodajPotworka2();
			pakietProszePotw.wyslijPakiet(this);
			
			PakietDodajWiezyczke2 pakietProszeWiez = new PakietDodajWiezyczke2();
			pakietProszeWiez.wyslijPakiet(this);
			
		
			while(true){
				byte[] dane = new byte[1024];
				DatagramPacket packet = new DatagramPacket(dane, dane.length,address,1338);
	            try {
	                socket.receive(packet);
	            } catch (IOException e) {
	                e.printStackTrace();
	            }
	            analizaPakietu(packet.getData(),packet.getAddress(),packet.getPort());
			}
			
			//socket.close();
		} catch (SocketException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	
	}
}
