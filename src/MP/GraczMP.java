package MP;

import java.net.InetAddress;

public class GraczMP {
	public InetAddress ipAddress;
	public int port;
	public String username;
	public int index;
	
	public GraczMP(InetAddress ipAddress, int port, String username, int index) {
		this.ipAddress = ipAddress;
		this.port = port;
		this.username = username;
		this.index = index;
	}
	
	
}
