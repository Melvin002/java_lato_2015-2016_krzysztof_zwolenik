package MP;

public class PakietKasa extends Pakiet {

	public int kasa;
	
	public PakietKasa(int czas) {
		super(Pakiet.KASA);
		this.kasa = czas;
	}
	public PakietKasa(byte [] dane) {
		super(Pakiet.KASA);
		String [] informacje = odczytajInf(dane).split(",");
		this.kasa = Integer.parseInt(informacje[0]);
	}

	@Override
	public byte[] getDane() {
		
		return ("10" + kasa).getBytes();
	}

	@Override
	public void wyslijPakiet(WatekUDP server) {
		server.wyslijDoWyszystkich(getDane());
	}

	@Override
	public void wyslijPakiet(KlientGry client) {
		client.wyslijPakiet(getDane());
		
	}
	
}
