package MP;

import java.awt.Point;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectInputStream;
import java.io.ObjectOutput;
import java.io.ObjectOutputStream;
import java.util.ArrayList;

import TD.Potworek;


public class PakietPrzeslijLinie extends Pakiet {

	ArrayList<Point> p;
	byte [] serLinie = new byte[1024];
	public PakietPrzeslijLinie(byte [] dane) {
		super(Pakiet.LINIEATK);
		byte [] trimmedDane = new byte[1024];
		System.arraycopy(dane, 2, trimmedDane, 0, dane.length - 2);
		ByteArrayInputStream bais = new ByteArrayInputStream(trimmedDane);
		ObjectInput in = null;
		
		try {
			in = new ObjectInputStream(bais);
			p = (ArrayList<Point>) in.readObject();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	public PakietPrzeslijLinie(ArrayList<Point> p) {
		super(Pakiet.LINIEATK);
		
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		ObjectOutput out = null;
		
		try {
			out = new ObjectOutputStream(baos);
			out.writeObject(p);
			serLinie = baos.toByteArray();
			baos.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@Override
	public void wyslijPakiet(WatekUDP server) {
		server.wyslijDoWyszystkich(getDane());	
	}

	@Override
	public byte[] getDane() {
		String pre = "07";
		byte [] preBytes = pre.getBytes();
		byte [] output = new byte [preBytes.length + serLinie.length]; 
		System.arraycopy(preBytes, 0, output, 0, preBytes.length);
		System.arraycopy(serLinie, 0, output, preBytes.length, serLinie.length);
		return output;
	}
	@Override
	public void wyslijPakiet(KlientGry client) {
		client.wyslijPakiet(getDane());
	}

}
