package MP;

import TD.Wiezyczka.TypWierzyczki;

public class PakietLogin extends Pakiet{

	String username;
	Integer index;
	
	public PakietLogin(String username) {
		super(Pakiet.LOGIN); 
		this.username = username;
	}	
	public PakietLogin(byte [] dane) {
		super(Pakiet.LOGIN);
		String [] informacje = odczytajInf(dane).split(",");
		this.username = informacje[0];	
	}
	public PakietLogin(byte [] dane,boolean a) {
		super(Pakiet.LOGIN);
		String [] informacje = odczytajInf(dane).split(",");
		this.index = Integer.parseInt(informacje[0]);	
	}

	@Override
	public void wyslijPakiet(WatekUDP server) {
		//server.wyslijDoWyszystkich(data);
	}

	@Override
	public void wyslijPakiet(KlientGry client) {
		client.wyslijPakiet(getDane());	
	}

	@Override
	public byte[] getDane() {
		return ("00" + this.username).getBytes();
	}

}
