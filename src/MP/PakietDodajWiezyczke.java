package MP;

import java.util.concurrent.atomic.AtomicInteger;

import TD.Wiezyczka;
import TD.Wiezyczka.TypWierzyczki;

public class PakietDodajWiezyczke extends Pakiet {
	static AtomicInteger count = new AtomicInteger(0);
	int x,y,id;
	TypWierzyczki typ;
	public PakietDodajWiezyczke(byte [] dane) {
		super(Pakiet.DODAJWIEZ);
		String [] informacje = odczytajInf(dane).split(",");
		this.x = Integer.parseInt(informacje[0]);
		this.y = Integer.parseInt(informacje[1]);
		this.typ = TypWierzyczki.valueOf(informacje[2]);	
		this.id = Integer.parseInt(informacje[3]);
	}
	public PakietDodajWiezyczke(int x, int y, TypWierzyczki typ) {
		super(Pakiet.DODAJWIEZ);
		this.x = x;
		this.y = y;
		this.typ = typ;
		this.id = count.incrementAndGet();
	}
	

	@Override
	public void wyslijPakiet(WatekUDP server) {
		server.wyslijDoWyszystkich(getDane());	
	}

	@Override
	public byte[] getDane() {
		return ("02" + this.x + "," + this.y + "," + this.typ + "," + this.id).getBytes();
	}
	@Override
	public void wyslijPakiet(KlientGry client) {
		client.wyslijPakiet(getDane());
	}

}
