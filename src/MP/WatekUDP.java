package MP;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectInputStream;
import java.io.ObjectOutput;
import java.io.ObjectOutputStream;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.SocketException;
import java.util.ArrayList;
import java.util.List;

import TD.OknoGry;
import TD.Potworek;
import TD.Wiezyczka;


public class WatekUDP extends Thread{
	
	ArrayList<Integer> indeksy; 
	public List<GraczMP> playerList = null;
	private DatagramSocket socket = null;
	private int port;
	private StronaSerwera stronaSerwera;
	

	public WatekUDP(int port, StronaSerwera stronaSerwera){
		
		indeksy = new ArrayList<Integer>();
		playerList = new ArrayList<GraczMP>();
		this.port = port;
		this.stronaSerwera = stronaSerwera;
		try {
			socket = new DatagramSocket(this.port);
		} catch (SocketException e) {
			socket.close();
			e.printStackTrace();
		}
	}
	public void run(){
		while(true){
			
			byte [] buf = new byte[256];
			DatagramPacket packet = new DatagramPacket(buf, buf.length);
			
			try{	
				socket.receive(packet);	
				
			}catch(IOException e){
				socket.close();
				e.printStackTrace();
			}
			this.analizaPakietu(packet.getData(), packet.getAddress(), packet.getPort());
			
		}	
	}
	private void analizaPakietu(byte[] dane, InetAddress address, int port){
		
		String informacja = new String(dane).trim();
		int typPakietu = Integer.parseInt(informacja.substring(0,2));
		
		Pakiet pakiet = null;
		
		switch(typPakietu){
		case Pakiet.LOGIN:
			pakiet = new PakietLogin(dane);
			PakietLogin p1 = (PakietLogin) pakiet;

			GraczMP gracz = new GraczMP(address, port, p1.username,sprawdzanieWolnegoIndeksu());
			playerList.add(gracz);
			//System.out.println(gracz.index);
			
			byte [] resp = ("00" + gracz.index).getBytes();
			wyslij(resp, address, port);
			
			break;
		case Pakiet.ZEGAR:
			pakiet = new PakietZegar(dane);
			PakietZegar pz = (PakietZegar) pakiet;
			stronaSerwera.zegar = pz.czas;
				stronaSerwera.pieniadze += 250;
				PakietKasa pakietKasa = new PakietKasa(stronaSerwera.pieniadze);
				pakietKasa.wyslijPakiet(this);
			pakiet.wyslijPakiet(this);
			break;
		case Pakiet.DODAJWIEZ:
			pakiet = new PakietDodajWiezyczke(dane);
			PakietDodajWiezyczke p2 = (PakietDodajWiezyczke) pakiet;
			stronaSerwera.wiezyczki.add(new Wiezyczka(p2.x,p2.y, p2.typ, p2.id));
			//
			PakietDodajWiezyczke pakiet2 = new PakietDodajWiezyczke(dane);
			pakiet2.wyslijPakiet(this);
			
			//sprawa pieniezna
			stronaSerwera.pieniadze -= 100;
			PakietKasa pakietKasa1 = new PakietKasa(stronaSerwera.pieniadze);
			pakietKasa1.wyslijPakiet(ServerGry.server);
			
			break;
		case Pakiet.USUNWIEZ:
			pakiet = new PakietKasujWiezyczke(dane);
			PakietKasujWiezyczke p3 = (PakietKasujWiezyczke) pakiet;
			Wiezyczka w = new Wiezyczka(p3.x, p3.y, p3.typ);
			
    		stronaSerwera.TabMap[p3.y][p3.x] = 1;
    		
    		stronaSerwera.wiezyczki.remove(w);
    		
			PakietKasujWiezyczke pakiet3 = new PakietKasujWiezyczke(dane);
			pakiet3.wyslijPakiet(this);
			
			//sprawa pieniezna
			stronaSerwera.pieniadze += 100;
			PakietKasa pakietKasa2 = new PakietKasa(stronaSerwera.pieniadze);
			pakietKasa2.wyslijPakiet(ServerGry.server);
				
			break;
		case Pakiet.MALUNKI:
			PakietMalowanie p4 = new PakietMalowanie(dane);
			p4.wyslijPakiet(this);
			break;
		case Pakiet.KASA:
			byte [] respKasa = ("10" + stronaSerwera.pieniadze).getBytes();
			wyslij(respKasa, address, port);
			break;
		case Pakiet.SERDUSZKA:
			byte [] respSerc = ("11" + stronaSerwera.serduszka).getBytes();
			wyslij(respSerc, address, port);
			break;
		case Pakiet.PKT:
			byte [] respPkt = ("12" + stronaSerwera.punkty).getBytes();
			wyslij(respPkt, address, port);
			break;
		case Pakiet.DODAJPOT2:
			for(Potworek m:stronaSerwera.potworki){
				byte [] serPotworek = new byte[1024];
				
				ByteArrayOutputStream baos = new ByteArrayOutputStream();
				ObjectOutput out = null;
				
				try {
					out = new ObjectOutputStream(baos);
					out.writeObject(m);
					serPotworek = baos.toByteArray();
					baos.close();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
				String pre = "13";
				byte [] preBytes = pre.getBytes();
				byte [] output = new byte [preBytes.length + serPotworek.length]; 
				System.arraycopy(preBytes, 0, output, 0, preBytes.length);
				System.arraycopy(serPotworek, 0, output, preBytes.length, serPotworek.length);
				
				wyslij(output, address, port);

			}
			break;
		case Pakiet.DODAJWIEZ2:
			for(Wiezyczka m:stronaSerwera.wiezyczki){
				byte [] respWiez = ("14" + m.getX()/50 + "," + m.getY()/50 + "," + m.getTypWierzyczki() + "," + m.id).getBytes();
				wyslij(respWiez, address, port);
			}
			
			break;
		}
		

	}
	public void wyslij(byte[] dane, InetAddress address, int port) {
		DatagramPacket packet = new DatagramPacket(dane, dane.length, address, port);
		try {
            socket.send(packet);
        } catch (IOException e) {
        	socket.close();
            e.printStackTrace();
        }
	}
    public void wyslijDoWyszystkich(byte[] data) {
        for (GraczMP p : playerList) {
            wyslij(data, p.ipAddress, p.port);
        }
    }
    private int sprawdzanieWolnegoIndeksu(){
		int test = 0;
		boolean powtarzaSie = false;
		while(true){
			for(Integer i:indeksy){
				if(i == test)
					powtarzaSie = true;
			}
			if(powtarzaSie == true){
				test++;
				powtarzaSie = false;
			}
			else{
				indeksy.add(test);
				return test;
			}
		}
    }
    
}
