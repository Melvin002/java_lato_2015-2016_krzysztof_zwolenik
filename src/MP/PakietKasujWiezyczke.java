package MP;

import TD.Wiezyczka;
import TD.Wiezyczka.TypWierzyczki;

public class PakietKasujWiezyczke extends Pakiet {

	int x,y;
	TypWierzyczki typ;
	public PakietKasujWiezyczke(byte [] dane) {
		super(Pakiet.USUNWIEZ);
		String [] informacje = odczytajInf(dane).split(",");
		this.x = Integer.parseInt(informacje[0]);
		this.y = Integer.parseInt(informacje[1]);
		this.typ = TypWierzyczki.valueOf(informacje[2]);	
	}
	public PakietKasujWiezyczke(int x, int y, TypWierzyczki typ) {
		super(Pakiet.USUNWIEZ);
		this.x = x;
		this.y = y;
		this.typ = typ;
	}

	@Override
	public void wyslijPakiet(WatekUDP server) {
		server.wyslijDoWyszystkich(getDane());	
	}

	@Override
	public byte[] getDane() {
		return ("03" + this.x + "," + this.y + "," + this.typ).getBytes();
	}
	@Override
	public void wyslijPakiet(KlientGry client) {
		client.wyslijPakiet(getDane());
	}

}
