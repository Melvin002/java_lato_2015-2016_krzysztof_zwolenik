package MP;

import java.io.IOException;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.util.ArrayList;

public class WatekTCP extends Thread{

	private Socket socket = null;
	private int [][] TabMap;
	private ArrayList<String> przeciwnicy;
	
	public WatekTCP(Socket socket, int[][] TabMap, ArrayList<String> przeciwnicy2){
		this.socket = socket;
		this.TabMap = TabMap;
		this.przeciwnicy = przeciwnicy2;
	}
	public void run(){
		try( ObjectOutputStream out = new ObjectOutputStream(socket.getOutputStream()) ){
			//
			for(int i = 0; i<10; i++){
				for(int j = 0; j<17; j++){
					out.writeInt(TabMap[i][j]);
					out.flush();
				}
			}
			
			out.writeObject(przeciwnicy);
			out.flush();
			
			//
            //socket.close();
		}catch (IOException e) {
			e.printStackTrace();
		} finally {
			if(!socket.isClosed())
				try {
					socket.close();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
		}
		
	}
	
}
