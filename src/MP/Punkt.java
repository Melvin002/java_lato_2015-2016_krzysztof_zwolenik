package MP;

import java.awt.Color;
import java.awt.Point;
import java.io.Serializable;

public class Punkt implements Serializable{
	int x,y;
	int alfa;
	int r,g,b;
	Color c;
	public Punkt(int x, int y,Color c){
		this.x = x;
		this.y = y;
		this.c = c;
		r = c.getRed();
		g = c.getGreen();
		b = c.getBlue();
		alfa = c.getAlpha() * 5;
	}
	public Color decay(){
		if(alfa > 0){
			alfa--;
			c = new Color(c.getRed(),c.getGreen(),c.getBlue(),alfa/5);
		}
		return c;
	}
}
