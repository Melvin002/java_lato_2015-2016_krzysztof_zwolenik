package MP;
import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Font;
import java.awt.FontFormatException;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.Point;
import java.awt.Stroke;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectInputStream;
import java.io.ObjectOutput;
import java.io.ObjectOutputStream;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Scanner;
import java.util.Stack;
import java.util.concurrent.atomic.AtomicInteger;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;
import javax.swing.Timer;

import TD.Potworek;
import TD.Wiezyczka;
import TD.Potworek.TypPotworka;
import TD.Wiezyczka.TrybStrzelania;
import TD.Wiezyczka.TypWierzyczki;

//setSize(867,540);
public class StronaSerwera implements ActionListener{

	
	static AtomicInteger count = new AtomicInteger(0);
	public int [][] TabMap = new int[10][17]; //[y][x]
	private boolean stanDAD = false;
	private int myszx, myszy, dyskretneX, dyskretneY;
	ArrayList<Wiezyczka> wiezyczki;
	ArrayList<Potworek> potworki;
	ArrayList<Point> punktyLiniAtaku;
	Wiezyczka.TypWierzyczki jakityp;
	Wiezyczka.TrybStrzelania trybStrzelania;
	Wiezyczka WiezyczkaWmenu = null;
	Font FontGra;
	Stack<String> stosikPrzeciwnikow;
	int licznikiteracji = 1; //wydluzenie czasu wyswietlania sie promieni laserow
	int zegar = 1200; // czas w sekundach= wart/40
	private Timer timer1;
	int pieniadze, serduszka, punkty;
	Image image1, image2, image3, serce, clock, dollar, score;
	JButton bStart;
	ArrayList<String> przeciwnicy;
	
	public StronaSerwera()  throws IOException{
		try {
			//InputStream in = getClass().getResourceAsStream("res/FontGra.ttf");
			FontGra = Font.createFont(Font.TRUETYPE_FONT, new File("FontGra.ttf")).deriveFont(20f);
			
		} catch (FontFormatException e) {
			e.printStackTrace();
		}
		jakityp = Wiezyczka.TypWierzyczki.DZIALO;
		WczytajMape();
		wiezyczki = new ArrayList<Wiezyczka>();
		potworki = new ArrayList<Potworek>();
		punktyLiniAtaku = new ArrayList<Point>();
		stosikPrzeciwnikow = new Stack<String>();
		wczytajPrzeciwnikow(1);
		pieniadze = 1000;
		
		serduszka = 10;
		//

		//
        timer1 = new Timer(25, this);
        timer1.start();
		ImageIcon ii1 = new ImageIcon("clock.png");
		clock = ii1.getImage();
		ImageIcon ii2 = new ImageIcon("heart.png");
		serce = ii2.getImage();
		ImageIcon ii3 = new ImageIcon("dollar.png");
		dollar = ii3.getImage();
		ImageIcon ii4 = new ImageIcon("score.png");
		score = ii4.getImage();
		
	}
	private void wczytajPrzeciwnikow(int i) throws IOException{
		
		String line = przeciwnicy.get(i - 1);
		Scanner scanner = new Scanner(line);
		while(scanner.hasNext()){
				stosikPrzeciwnikow.add(scanner.next());
		}
		scanner.close();
	}
	public void WczytajMape() throws IOException{
		przeciwnicy = (ArrayList<String>) Files.readAllLines(Paths.get("Poziomy"));
		Scanner scanner = new Scanner(new File("mapa.map"));
		
		for(int i = 0; i<10; i++){
			for(int j = 0; j<17; j++){
				TabMap[i][j] = scanner.nextInt();
			}
		}
		scanner.close();
		
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		
		boolean zieloneSwiatlo = true;
		Iterator <Potworek> i1 = potworki.iterator();
		while(i1.hasNext()){
			Potworek m = (Potworek) i1.next();
			if(TabMap[m.getY()/50][m.getX()/50] == 4 || TabMap[(m.getY() + 49)/50][(m.getX() + 49)/50] == 4)
				zieloneSwiatlo = false;
		}
		if(zegar == 0 && zieloneSwiatlo == true && !stosikPrzeciwnikow.empty()){
			Potworek tempP = new Potworek(Potworek.TypPotworka.valueOf(stosikPrzeciwnikow.pop()),count.incrementAndGet());
			potworki.add(tempP);
			PakietDodajPotworka pakiet = new PakietDodajPotworka(tempP);
			pakiet.wyslijPakiet(ServerGry.server);
		}
		
		Iterator <Wiezyczka> w = wiezyczki.iterator();
		while(w.hasNext()){
			Wiezyczka m = (Wiezyczka) w.next();
			m.WarunekZniszczenia(potworki, punktyLiniAtaku, ServerGry.server);
		}
		
		PakietPrzeslijLinie pakietLinie = new PakietPrzeslijLinie(punktyLiniAtaku);
		pakietLinie.wyslijPakiet(ServerGry.server);
		
		punktyLiniAtaku.clear();
		Iterator <Potworek> i2 = potworki.iterator();
		while(i2.hasNext()){
			Potworek m = (Potworek) i2.next();

			if(m.move(TabMap)){
				serduszka--;
				PakietSerduszka pakietSer = new PakietSerduszka(serduszka);
				pakietSer.wyslijPakiet(ServerGry.server);
			}

			
			if(m.getZycie() < 0){
				i2.remove();
				pieniadze += 50;
				PakietKasa pakietKasa = new PakietKasa(pieniadze);
				pakietKasa.wyslijPakiet(ServerGry.server);
				
				punkty+= 100;
				PakietPunkty pakietPkt = new PakietPunkty(punkty);
				pakietPkt.wyslijPakiet(ServerGry.server);
			}
			
		}
		if(zegar%40 == 0 && zegar != 0){
			PakietZegar pakiet1 = new PakietZegar(zegar);
			pakiet1.wyslijPakiet(ServerGry.server);		
		}
		if(zegar > 0)
			zegar--;
		else if(stosikPrzeciwnikow.empty()){
			try {
				wczytajPrzeciwnikow(2);
			} catch (IOException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			PakietZegar pakiet = new PakietZegar(1200);
			pakiet.wyslijPakiet(ServerGry.server);
			zegar = 1200;
		}
	}
}
