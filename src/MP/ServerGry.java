package MP;

import java.io.IOException;
import java.net.ServerSocket;

public class ServerGry extends Thread{
	
	
	private int port = 1337;
	public StronaSerwera stronaSerwera;
	public static WatekUDP server;

	public ServerGry(StronaSerwera stronaSerwera){
		this.stronaSerwera = stronaSerwera;
		this.start();
	}
	public void run(){
		try ( ServerSocket SocketTCP = new ServerSocket(port) ){
			
			//stronaSerwera = new StronaSerwera();
			server = new WatekUDP(port + 1,stronaSerwera);
			server.start();
			
			
			while(true){
				new WatekTCP(SocketTCP.accept(), stronaSerwera.TabMap, stronaSerwera.przeciwnicy).start();
			}
			
		}catch (IOException e) {
			e.printStackTrace();
		}
	}
}
