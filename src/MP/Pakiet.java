package MP;

public abstract class Pakiet {
	
	public static final int LOGIN = 00;
	public static final int ZEGAR = 01;
	public static final int DODAJWIEZ = 02;
	public static final int USUNWIEZ = 03;
	public static final int DODAJPOT = 04;
	public static final int MALUNKI = 05;
	public static final int OBRAZENIA = 06;
	public static final int LINIEATK = 07;
	public static final int KASA = 10;
	public static final int SERDUSZKA = 11;
	public static final int PKT = 12;
	public static final int DODAJPOT2 = 13;
	public static final int DODAJWIEZ2 = 14;


	
	public byte idPakietu;
	
	public Pakiet(int idPakietu){
		this.idPakietu = (byte) idPakietu;
	}
	public abstract void wyslijPakiet(WatekUDP server);
	public abstract void wyslijPakiet(KlientGry client);
	public abstract byte[] getDane();
	public String odczytajInf(byte[] dane) {
		String informacja = new String(dane).trim();
		return informacja.substring(2);
	}
}
