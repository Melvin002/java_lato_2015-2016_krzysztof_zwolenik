package MP;

public class PakietZegar extends Pakiet {

	public int czas;
	
	public PakietZegar(int czas) {
		super(Pakiet.ZEGAR);
		this.czas = czas;
	}
	public PakietZegar(byte [] dane) {
		super(Pakiet.ZEGAR);
		String [] informacje = odczytajInf(dane).split(",");
		this.czas = Integer.parseInt(informacje[0]);
	}

	@Override
	public byte[] getDane() {
		
		return ("01" + czas).getBytes();
	}

	@Override
	public void wyslijPakiet(WatekUDP server) {
		server.wyslijDoWyszystkich(getDane());
	}

	@Override
	public void wyslijPakiet(KlientGry client) {
		client.wyslijPakiet(getDane());
		
	}
	
}
