package MP;

import java.awt.Color;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectInputStream;
import java.io.ObjectOutput;
import java.io.ObjectOutputStream;

import TD.Potworek;
import TD.Wiezyczka.TypWierzyczki;


public class PakietMalowanie extends Pakiet {

	int x,y,r,g,b,alfa;

	public PakietMalowanie(byte [] dane) {
		super(Pakiet.MALUNKI);
		String [] informacje = odczytajInf(dane).split(",");
		this.x = Integer.parseInt(informacje[0]);
		this.y = Integer.parseInt(informacje[1]);
		this.r = Integer.parseInt(informacje[2]);
		this.g = Integer.parseInt(informacje[3]);
		this.b = Integer.parseInt(informacje[4]);
		this.alfa = Integer.parseInt(informacje[5]);
	}
	public PakietMalowanie(Punkt p) {
		super(Pakiet.MALUNKI);
		this.x = p.x;
		this.y = p.y;
		this.r = p.r;
		this.g = p.g;
		this.b = p.b;
		this.alfa = p.alfa;
	}

	@Override
	public void wyslijPakiet(WatekUDP server) {
		server.wyslijDoWyszystkich(getDane());	
	}

	@Override
	public byte[] getDane() {
		return ("05" + this.x + "," + this.y + "," + this.r + "," + this.g + "," + this.b + "," + this.alfa).getBytes();

	}
	@Override
	public void wyslijPakiet(KlientGry client) {
		client.wyslijPakiet(getDane());
	}

}
