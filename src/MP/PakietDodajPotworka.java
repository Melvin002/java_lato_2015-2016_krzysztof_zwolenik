package MP;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectInputStream;
import java.io.ObjectOutput;
import java.io.ObjectOutputStream;

import TD.Potworek;


public class PakietDodajPotworka extends Pakiet {

	Potworek p;
	byte [] serPotworek = new byte[1024];
	public PakietDodajPotworka(byte [] dane) {
		super(Pakiet.DODAJPOT);
		byte [] trimmedDane = new byte[1024];
		System.arraycopy(dane, 2, trimmedDane, 0, dane.length - 2);
		ByteArrayInputStream bais = new ByteArrayInputStream(trimmedDane);
		ObjectInput in = null;
		
		try {
			in = new ObjectInputStream(bais);
			p = (Potworek) in.readObject();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	public PakietDodajPotworka(Potworek p) {
		super(Pakiet.DODAJPOT);
		
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		ObjectOutput out = null;
		
		try {
			out = new ObjectOutputStream(baos);
			out.writeObject(p);
			serPotworek = baos.toByteArray();
			baos.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@Override
	public void wyslijPakiet(WatekUDP server) {
		server.wyslijDoWyszystkich(getDane());	
	}

	@Override
	public byte[] getDane() {
		String pre = "04";
		byte [] preBytes = pre.getBytes();
		byte [] output = new byte [preBytes.length + serPotworek.length]; 
		System.arraycopy(preBytes, 0, output, 0, preBytes.length);
		System.arraycopy(serPotworek, 0, output, preBytes.length, serPotworek.length);
		return output;
	}
	@Override
	public void wyslijPakiet(KlientGry client) {
		client.wyslijPakiet(getDane());
	}

}
